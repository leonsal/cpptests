#include "RtAudio.h"
#include <iostream>
#include <cstdlib>


// Two-channel sawtooth wave generator.
int saw( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
         double streamTime, RtAudioStreamStatus status, void *userData ) {

    unsigned int i, j;
    double *buffer = (double *) outputBuffer;
    double *lastValues = (double *) userData;

    if (status) {
        std::cout << "Stream underflow detected!" << std::endl;
    }

    // Write interleaved audio data.
    for (i = 0; i < nBufferFrames; i++) {
        for (j = 0; j < 2; j++) {
            *buffer++ = lastValues[j];
            lastValues[j] += 0.005 * (j+1+(j*0.1));
            if ( lastValues[j] >= 1.0 ) {
                lastValues[j] -= 2.0;
            }
        }
    }
    return 0;
}


int main()  {

    RtAudio dac;

    unsigned int devices = dac.getDeviceCount();
    if (devices < 1) {
        std::cout << "\nNo audio devices found!\n";
        exit(0);
    }

    // Scan through devices for various capabilities
    RtAudio::DeviceInfo info;
    for (int i = 0; i < devices; i++) {
        info = dac.getDeviceInfo(i);
        if (info.probed == true ){
            std::cout << "device: " << i << " (" << info.name << ")\n";
            std::cout << "\tmaximum output channels: " << info.outputChannels << "\n";
            std::cout << "\tmaximum input channels : " << info.inputChannels << "\n";
            std::cout << "\tmaximum duplex channels: " << info.duplexChannels << "\n";
            std::cout << "\tdefault output.........: " << info.isDefaultOutput << "\n";
            std::cout << "\tdefault input..........: " << info.isDefaultInput << "\n";
            std::cout << "\tsample rates...........: ";
            for (auto r : info.sampleRates) {
                std::cout << r << ",";
            }
            std::cout << "\n";
            std::cout << "\tpreferred sample rate..: " << info.preferredSampleRate << "\n";
        }
    }

    RtAudio::StreamParameters parameters;
    parameters.deviceId = dac.getDefaultOutputDevice();
    std::cout << "default device: " << parameters.deviceId << "\n";
    parameters.nChannels = 2;
    parameters.firstChannel = 0;
    unsigned int sampleRate = 44100;
    unsigned int bufferFrames = 2*256;
    double data[2];
    try {
        dac.openStream( &parameters, NULL, RTAUDIO_FLOAT64,
                        sampleRate, &bufferFrames, &saw, (void*)&data );
        dac.startStream();
    }
    catch ( RtAudioError& e ) {
        e.printMessage();
        exit( 0 );
    }

    char input;
    std::cout << "bufferFrames:" << bufferFrames << std::endl;
    std::cout << "\nPlaying ... press <enter> to quit.\n";
    std::cin.get(input);
    try {
        // Stop the stream
        dac.stopStream();
    }
    catch (RtAudioError& e) {
        e.printMessage();
    }
    if (dac.isStreamOpen())  {
        dac.closeStream();
    }
    return 0;
}


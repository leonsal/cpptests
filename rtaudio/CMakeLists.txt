cmake_minimum_required(VERSION 3.0 FATAL_ERROR)

project(rtaudio_test)

option(BUILD_SHARED_LIBS "Build as shared library" OFF)
option(RTAUDIO_BUILD_PYTHON "Build PyRtAudio python bindings" OFF)
set(RTAUDIO_BUILD_TESTING OFF)
add_subdirectory(rtaudio rtaudio)
include_directories(rtaudio)

set (CMAKE_CXX_STANDARD 17)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR})
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_executable(rtaudio_test main.cpp)
target_link_libraries(rtaudio_test rtaudio)




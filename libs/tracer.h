#pragma once
#include <chrono>
#include <thread>
#include <string>
#include <vector>
#include <memory>
#include <atomic>
#include <mutex>
#include "spinlock.h"

namespace sdr {


class Tracer {

public:
    ~Tracer() = default;
    Tracer(const Tracer&) = delete;
    Tracer(Tracer&&) = delete;
    Tracer& operator=(const Tracer&) = delete;
    Tracer& operator=(Tracer&&) = delete;
    static Tracer& get();

    enum Scope {
        Default = ' ',
        Global  = 'g',
        Process = 'p',
        Thread  = 't'
    };

    size_t eventCapacity() const;
    void setEventCapacity(size_t max);
    void enableConsole(bool enable);
    void enableRecord(bool enable);
    void setConsoleCats(const std::vector<std::string>& cats);
    void setRecordCats(const std::vector<std::string>& cats);
    void traceBegin(const std::string& name, const std::string& cat);
    void traceEnd(const std::string& name, const std::string& cat);
    void traceInstant(const std::string& name, const std::string& cat, Scope s=Default);
    size_t eventCount() const;
    void dump(std::ostream& out);
    void clear();

private:
    Tracer();
    bool checkConsole(const std::string& cat);
    bool checkRecord(const std::string& cat);
    void append(const std::string& name, const std::string& cat, char ph, Scope=Default);
    void print(std::ostream& out, char type, const std::string& name, const std::string& cat);
    struct Event {
        std::string     name;
        std::string     cat;
        int             pid;
        int             tid;
        int             ph;
        Scope           scope;
        std::chrono::time_point<std::chrono::high_resolution_clock> ts;
    };
    bool mConsole{false};
    bool mRecord{false};
    std::vector<std::string> mConsoleCats;
    std::vector<std::string> mRecordCats;
    int mPid{};
    std::vector<Event> mEvents;
    std::atomic<int> mNextTid{1};
    std::atomic<std::size_t> mNextIndex{0};
    spinlock mNextIndexLock;
    std::mutex mMutConsole;
};

} // namespace


// Macros to conditionally trace events
#ifdef SDRKIT_TRACER
    #define TRACE_BEGIN(NAME,CAT) sdr::Tracer::get().traceBegin(NAME,CAT)
    #define TRACE_END(NAME,CAT)   sdr::Tracer::get().traceEnd(NAME,CAT)
    #define TRACE_INSTANT(NAME,CAT,SCOPE) sdr::Tracer::get().traceInstant(NAME,CAT,SCOPE)
#else
    #define TRACE_BEGIN(NAME,CAT)
    #define TRACE_END(NAME,CAT)
    #define TRACE_INSTANT(NAME,CAT,SCOPE)
#endif


#pragma once
#include <atomic>

// https://rigtorp.se/spinlock/https://rigtorp.se/spinlock/
struct spinlock {

    std::atomic<bool> lock_ = {false};

    inline void lock() noexcept {

        for (;;) {
            // Optimistically assume the lock is free on the first try
            if (!lock_.exchange(true, std::memory_order_acquire)) {
                return;
            }
            // Wait for lock to be released without generating cache misses
            while (lock_.load(std::memory_order_relaxed)) {
                // Issue X86 PAUSE or ARM YIELD instruction to reduce contention between
                // hyper-threads
                // __builtin_ia32_pause();
            }
        }
    }

    inline void unlock() noexcept {

        lock_.store(false, std::memory_order_release);
    }
};

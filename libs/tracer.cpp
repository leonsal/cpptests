#include <chrono>
#include <iostream>
#include <algorithm>
#if defined(__linux__)
#include <sys/types.h>
#include <unistd.h>
#elif defined(_WIN32)
#include <windows.h>
#endif
#include "tracer.h"


namespace sdr {

// Thread local generated thread id
static thread_local int gThreadId = 0;

/**
 * Constructor
 */
Tracer::Tracer() {

#if defined(__linux__)
    mPid = getpid();
#endif
#if defined(_WIN32)
    mPid = GetCurrentProcessId();
#endif
}

/**
 * Get instance of global Tracer (singleton)
 */
Tracer& Tracer::get() {

    static Tracer tracer;   // thread safe
    return tracer;
}

size_t Tracer::eventCapacity() const {

    return mEvents.size();
}

void Tracer::setEventCapacity(size_t max) {

    mEvents.resize(max);
}

/**
 * Enable/disable sending traces to the console.
 */
void Tracer::enableConsole(bool enable) {

    mConsole = enable;
}

/**
 * Enable/disable recording traces to the trace buffer
 */
void Tracer::enableRecord(bool enable) {

    mRecord = enable;
}

/**
 * Sets list of event categories which will be sent to the console.
 * An empty vector enable all categories.
 */
void Tracer::setConsoleCats(const std::vector<std::string>& cats) {

    mConsoleCats = cats;
}

/**
 * Sets list of event categories which will be recorded into the trace buffer.
 * An empty vector enable all categories.
 */
void Tracer::setRecordCats(const std::vector<std::string>& cats) {

    mRecordCats = cats;
}

/**
 * Adds a 'begin' event to the trace buffer with the specified name and category
 */
void Tracer::traceBegin(const std::string& name, const std::string& cat) {

    append(name, cat, 'B');
}

/**
 * Adds an 'end' event to the trace buffer corresponding to a previous added 'begin' event
 */
void Tracer::traceEnd(const std::string& name, const std::string& cat) {

    append(name, cat, 'E');
}

/**
 * Adds an 'instant' event to the trace buffer
 */
void Tracer::traceInstant(const std::string& name, const std::string& cat, Scope scope) {

    append(name, cat, 'i', scope);
}

/**
 * Returns the current number of events in the trace buffer
 */
size_t Tracer::eventCount() const {

    return mNextIndex;
}

/**
 * Dumps the recorded events to the specified output stream using
 * the JSON Trace Event Format:
 * https://docs.google.com/document/d/1CvAClvFfyA5R-PhYUmn5OOQtYMH4h6I0nSsKchNAySU/preview#heading=h.yr4qxyxotyw
 */
void Tracer::dump(std::ostream& out) {

    using namespace std::chrono;
    out << "[";
    char buf[512];
    for (size_t i = 0; i < eventCount(); i++) {
        Event& ev = mEvents[i];
        auto ts = std::chrono::duration_cast<microseconds>(ev.ts.time_since_epoch()).count();
        snprintf(buf, sizeof(buf),
            R"({"name":"%s","cat":"%s","ph":"%c","ts":%lu,"pid":%d,"tid":%d)",
            ev.name.c_str(), ev.cat.c_str(), ev.ph, ts, ev.pid, ev.tid);
        out << buf;
        // Optional instant event extra parameter
        if (ev.scope == Default) {
           out << "}";
        } else {
            snprintf(buf, sizeof(buf), R"(,"s":"%c"})", (char)(ev.scope));
            out << buf;
        }
        if (i < eventCount() - 1) {
            out << ",\n";
        }
    }
    out << "]";
}

/**
 * Clear the events buffer
 */
void Tracer::clear() {

    mNextIndex = 0;
}

/**
 * Returns if trace with the specified category should be
 * printed to the console
 */
bool Tracer::checkConsole(const std::string& cat) {

    if (!mConsole) {
        return false;
    }
    if (mConsoleCats.empty()) {
        return true;
    }
    if (std::find(mConsoleCats.cbegin(), mConsoleCats.cend(), cat) != mConsoleCats.cend()) {
        return true;
    }
    return false;
}

/**
 * Returns if trace with the specified category should be recorded
 */
bool Tracer::checkRecord(const std::string& cat) {

    if (!mRecord) {
        return false;
    }
    if (mRecordCats.empty()) {
        return true;
    }
    if (std::find(mRecordCats.cbegin(), mRecordCats.cend(), cat) != mRecordCats.cend()) {
        return true;
    }
    return false;
}

void Tracer::append(const std::string& name, const std::string& cat, char ph, Scope scope) {

    auto now = std::chrono::high_resolution_clock::now();

    // Generates a unique id for the thread once
    if (gThreadId == 0) {
        gThreadId = mNextTid.fetch_add(1);
    }

    //if (checkConsole(cat) && ph == 'B') {
    if (checkConsole(cat)) {
        print(std::cout, ph, name, cat);
    }

    if (checkRecord(cat)) {
        // Get index for storage of next event
        mNextIndexLock.lock();
        size_t idx = mNextIndex;
        if (idx >= mEvents.size()) {
            mNextIndexLock.unlock();
            return;
        }
        mNextIndex++;
        mNextIndexLock.unlock();

        Event& ev = mEvents[idx];
        ev.name     = name;
        ev.cat      = cat;
        ev.ph       = ph;
        ev.pid      = mPid;
        ev.tid      = gThreadId;
        ev.scope    = scope;
        ev.ts       = now;
    }
}

void Tracer::print(std::ostream& out, char type, const std::string& name, const std::string& cat) {

    std::scoped_lock lock(mMutConsole);
    auto now = std::chrono::system_clock::now();
    auto time = std::chrono::system_clock::to_time_t(now);
    auto us = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch()) -
          std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch());
    tm* tm = localtime(&time);
    char buf[512];
    snprintf(buf, sizeof(buf),
        "%02d:%02d:%02d.%06ld-%02d-%c|%s|%s\n",
        tm->tm_hour, tm->tm_min, tm->tm_sec, us.count(), gThreadId, type, name.c_str(), cat.c_str());
    out << buf;
}

} // namespace


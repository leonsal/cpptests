#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>


int g1 = 0;
void increment_g1(int count) {

    for (int i = 0; i < count; ++i) {
        g1++;
    }
}


std::atomic<int> g2{};
void increment_g2(int count) {

    for (int i = 0; i < count; ++i) {
        g2++;
    }
}


int g3 = 0;
std::mutex mutg3;
void increment_g3(int count) {

    for (int i = 0; i < count; ++i) {
        mutg3.lock();
        g3++;
        mutg3.unlock();
    }
}

size_t exec(int nthreads, int count, void(*pfn)(int)) {

    auto start = std::chrono::high_resolution_clock::now();

    // Create threads
    std::vector<std::thread> threads;
    for (int t = 0; t < nthreads; ++t)  {
        std::thread tr(pfn, count);
        threads.push_back(std::move(tr));
    }

    // Wait for threads to finish
    for (int t = 0; t < threads.size(); t++) {
        threads[t].join();
    }

    // Returns elapsed time in microseconds
    auto end = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::microseconds>(end-start).count();
}


int main() {

    std::cout << "Atomic vs Mutex test\n";
    const int nthreads = 10;
    const int count = 100'000;

    auto time = exec(nthreads, count, increment_g1);
    std::cout << "g1=" << g1 << " time(us)=" << time << "\n";

    time = exec(nthreads, count, increment_g2);
    std::cout << "g2=" << g2 << " time(us)=" << time << "\n";

    time = exec(nthreads, count, increment_g3);
    std::cout << "g3=" << g3 << " time(us)=" << time << "\n";
}


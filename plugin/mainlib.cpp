#include <iostream>
#include "mainlib.h"


Main::Main(const std::string& msg): mMessage{msg} {}


std::string Main::message() const {

    return mMessage;
}


void Main::setMessage(const std::string& msg) {

    mMessage = msg;
}


void mainlib_print() {

    std::cout << "mainlib_print\n";
}


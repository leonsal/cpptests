#pragma once

#if defined __GNUC__
#   define __SYM_EXPORT     __attribute__((visibility("default")))
#   define __SYM_IMPORT     __attribute__((visibility("default")))
#elif _MSC_VER
#   define __SYM_EXPORT     __declspec(dllexport)
#   define __SYM_IMPORT     __declspec(dllimport)
#else
#   define __SYM_EXPORT
#   define __SYM_IMPORT
#endif

// MAIN_BUILD is set by CMake only when building main program modules
#ifdef MAIN_BUILD
#   define MAIN_API __SYM_EXPORT
#else
#   define MAIN_API __SYM_IMPORT
#endif

#include <string>

class MAIN_API Main {

    public:
        Main(const std::string& msg);
        std::string message() const;
        void setMessage(const std::string& msg);
    private: 
        std::string mMessage;
};



MAIN_API void mainlib_print();



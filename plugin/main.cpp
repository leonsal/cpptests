#include <iostream>
#include <string>
#include "plugins/plugin.h"


#ifdef _WIN32
#include <windows.h>

HMODULE open_plugin(const char* path) {
    return LoadLibraryA(path);
}

static void close_plugin(HMODULE mod) {
	FreeLibrary(mod);
}

void* get_proc(HMODULE mod, const char *proc) {

    return GetProcAddress(mod, proc);
}

static const char* path1 = "build_msvc_debug\\plugins\\plugin1.dll";
static const char* path2 = "build_msvc_debug\\plugins\\plugin2.dll";
#endif



#ifdef __linux__
#include <dlfcn.h>

void* open_plugin(const char* path) {
    return dlopen(path, RTLD_NOW);
}

static void close_plugin(void* mod) {
	dlclose(mod);
}

void* get_proc(void* handle, const char *proc) {

    return dlsym(handle, proc);
}

static const char* path1 = "build_gcc_debug/plugins/libplugin1.so";
static const char* path2 = "build_gcc_debug/plugins/libplugin2.so";
#endif


using plugin_create = Plugin* (const std::string& name);

int main() {

    std::cout << "loader\n";

    auto p1 = open_plugin(path1);
    if (p1 == nullptr) {
        std::cout << "Error loading:" << path1 << "\n";
        return 1;
    }

    plugin_create* create1 = (plugin_create*)get_proc(p1, "create");
    if (create1 == nullptr) {
        std::cout << "create1 function not found\n";
        return 1;
    }

    Plugin* pinst1 = create1("p1");
    pinst1->start();
    pinst1->stop();



    auto p2 = open_plugin(path2);
    if (p2 == nullptr) {
        std::cout << "Error loading:" << path2 << "\n";
        return 1;
    }

    plugin_create* create2 = (plugin_create*)get_proc(p2, "create");
    if (create2 == nullptr) {
        std::cout << "create1 function not found\n";
        return 1;
    }

    Plugin* pinst2 = create2("p2");
    pinst2->start();
    pinst2->stop();

    close_plugin(p1);
    close_plugin(p2);
}


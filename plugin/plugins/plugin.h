#pragma once
#include <string>

#if defined __GNUC__
#   define __SYM_EXPORT     __attribute__((visibility("default")))
#   define __SYM_IMPORT     __attribute__((visibility("default")))
#elif _MSC_VER
#   define __SYM_EXPORT     __declspec(dllexport)
#   define __SYM_IMPORT     __declspec(dllimport)
#else
#   define __SYM_EXPORT
#   define __SYM_IMPORT
#endif

// PLUGIN_BUILD is set by CMake only when building the plugins
#ifdef PLUGIN_BUILD
#   define PLUGIN_API __SYM_EXPORT
#else
#   define PLUGIN_API __SYM_IMPORT
#endif

class PLUGIN_API Plugin {

    public:
        virtual void start() = 0;
        virtual void stop() = 0;
};


extern "C" {

PLUGIN_API Plugin* create(const std::string& name);

};





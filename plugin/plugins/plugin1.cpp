#include <iostream>
#include "plugin.h"
#include "../mainlib.h"


class PluginOne : public Plugin {

    public:
        PluginOne(const std::string name): mName{name} {}

        void start() override {
            std::cout << "PluginOne:" << mName << " started\n";
        }

        void stop() override {
            std::cout << "PluginOne:" << mName << " stopped\n";
        }
    private:
        std::string mName;
};


Plugin* create(const std::string& name) {

    mainlib_print();
    auto m = new Main("main message");
    std::cout << "message:" << m->message() << "\n";
    delete m;
    return new PluginOne(name);
}




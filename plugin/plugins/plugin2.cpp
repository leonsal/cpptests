#include <iostream>
#include "plugin.h"

class PluginTwo : public Plugin {

    public:
        PluginTwo(const std::string name): mName{name} {}

        void start() override {
            std::cout << "PluginTwo:" << mName << " started\n";
        }

        void stop() override {
            std::cout << "PluginTwo:" << mName << " stopped\n";
        }
    private:
        std::string mName;
};

Plugin* create(const std::string& name) {

    return new PluginTwo(name);
}

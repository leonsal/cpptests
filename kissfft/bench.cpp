#include <iostream>
#include <complex>
#include <benchmark/benchmark.h>
#include "kiss_fft.h"

static void BM_FFT_forward(benchmark::State& state) {

    using Real = double;

    // Create input/output vectors
    const size_t N = 8*1024;
    std::vector<std::complex<Real>> inp(N, 0);
    for (size_t i = 0; i < N; i++) {
        inp[i] = std::complex<Real>((Real)i, -(Real)i);
    }
    std::vector<std::complex<Real>> out(N, 0);

    // Creates forward configuration
    kiss_fft_cfg cfg = kiss_fft_alloc(N, 0, nullptr, nullptr);

    // Timed
    for (auto _ : state) {
        kiss_fft(cfg, (kiss_fft_cpx*)(inp.data()), (kiss_fft_cpx*)(out.data()));
    }
}

// Register the function as a benchmark
BENCHMARK(BM_FFT_forward);

BENCHMARK_MAIN();


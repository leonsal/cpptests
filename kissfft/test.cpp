#include <vector>
#include <complex>
#include <cstdio>
#include "kiss_fft.h"

int main() {

    using Real = double;

    // Create input/output vectors
    const int N = 16;
    std::vector<std::complex<Real>> inp(N, 0);
    for (int i = 0; i < N; i++) {
        inp[i] = std::complex<Real>((Real)i, -(Real)i);
        printf("inp[%02d] = (%8.4f, %8.4f)\n", i, inp[i].real(), inp[i].imag());
    }
    printf("\n");
    std::vector<std::complex<Real>> out(N, 0);
    std::vector<std::complex<Real>> inv(N, 0);

    // Creates forward configuration, execute and print
    kiss_fft_cfg cfg1 = kiss_fft_alloc(N, 0, nullptr, nullptr);
    kiss_fft(cfg1, (kiss_fft_cpx*)(inp.data()), (kiss_fft_cpx*)(out.data()));
    for (int i = 0; i < N; i++) {
        printf("inp[%02d] = (%8.4f, %8.4f)\n", i, out[i].real(), out[i].imag());
    }
    printf("\n");

    // Calculates the inverse FFT, scales and prints
    kiss_fft_cfg cfg2 = kiss_fft_alloc(N, 1, nullptr, nullptr);
    kiss_fft(cfg2, (kiss_fft_cpx*)(out.data()), (kiss_fft_cpx*)(inv.data()));
    for (int i = 0; i < N; i++) {
        auto sreal = inv[i].real() / (Real)N;
        auto simag = inv[i].imag() / (Real)N;
        printf("inv[%02d] = (%8.4f, %8.4f)\n", i, sreal, simag);
    }
    printf("\n");
}



#include <iostream>
#include <benchmark/benchmark.h>
#include <fftw3.h>

static void BM_FFT_forward(benchmark::State& state) {

    // Setup
    const size_t N = 8*1024;

    // Allocate buffers
    fftw_complex* inp = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    fftw_complex* out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

    // Initialize input buffer
    for (int i = 0; i < N; i++) {
        inp[i][0] = (double)i;
        inp[i][1] = -(double)i;
    }

    // Create plan
    fftw_plan plan = fftw_plan_dft_1d(N, inp, out, FFTW_FORWARD, FFTW_ESTIMATE);

    // Timed
    for (auto _ : state) {
        fftw_execute(plan);
    }
}

// Register the function as a benchmark
BENCHMARK(BM_FFT_forward);

BENCHMARK_MAIN();


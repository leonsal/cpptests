#include <iostream>
#include <cstdio>
#include <fftw3.h>

int main() {

    std::cout << "fftw\n";

    fftw_complex *inp, *out, *inv;
    fftw_plan p;
    size_t N = 16;

    // Generates Input data
    inp = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    inv = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    for (int i = 0; i < N; i++) {
        inp[i][0] = (double)i;
        inp[i][1] = -(double)i;
    }
    for (int i = 0; i < N; i++) {
        printf("inp[%02d] = (%8.4f, %8.4f)\n", i, inp[i][0], inp[i][1]);
    }
    printf("\n");

    // Calculates the forward FFT and prints
    p = fftw_plan_dft_1d(N, inp, out, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(p);
    for (int i = 0; i < N; i++) {
        printf("out[%02d] = (%8.4f, %8.4f)\n", i, out[i][0], out[i][1]);
    }
    printf("\n");

    // Calculates the inverse FFT, scales and prints
    p = fftw_plan_dft_1d(N, out, inv, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(p);
    for (int i = 0; i < N; i++) {
        auto sreal = inv[i][0] / (double)N;
        auto simag = inv[i][1] / (double)N;
        printf("inv[%02d] = (%8.4f, %8.4f)\n", i, sreal, simag);
    }
    printf("\n");

    fftw_destroy_plan(p);
    fftw_free(inp);
    fftw_free(out);
}


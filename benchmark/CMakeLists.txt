cmake_minimum_required(VERSION 3.0 FATAL_ERROR)

project(bench)

find_package(Threads)

# Needs pkgconfig to find benchmark
#find_package(PkgConfig)

# Required benchmark library
#pkg_check_modules(benchmark benchmark)

set(CMAKE_BUILD_TYPE Release)
add_subdirectory(benchmark benchmark)
include_directories(benchmark/include)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR})
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# Demo bench
add_executable(bench bench.cpp)
target_link_libraries(bench benchmark pthread)

# Float vs Double
add_executable(float_double float_double.cpp)
target_link_libraries(float_double benchmark Threads::Threads)




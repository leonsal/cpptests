#!/bin/sh
git clone https://github.com/google/benchmark.git
rm -rf benchmark/.git

# Benchmark requires Google Test as a dependency. Add the source tree as a subdirectory.
git clone https://github.com/google/googletest.git benchmark/googletest
rm -rf benchmark/googletest/.git


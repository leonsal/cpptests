#include <iostream>
#include <benchmark/benchmark.h>
#include <vector>

const int size = 8*1024;

static void BM_FloatVector(benchmark::State& state) {

    // Setup
    std::vector<float> v1;
    v1.reserve(size);
    for (int i = 0; i < size; i++) {
        v1[i] = 300.0/(i+1); 
    }
    std::vector<float> v2;
    v2.reserve(size);
    for (int i = 0; i < size; i++) {
        v1[i] = 400.0/(i+1); 
    }
    std::vector<float> vr;
    vr.reserve(size);

    // Timed
    for (auto _ : state) {
        for (int i = 0; i < size; i++) {
            vr[i] = v1[i] * v2[i];
        }
    }
}
// Register the function as a benchmark
BENCHMARK(BM_FloatVector);

static void BM_DoubleVector(benchmark::State& state) {

    // Setup
    std::vector<double> v1;
    v1.reserve(size);
    for (int i = 0; i < size; i++) {
        v1[i] = 300.0/(i+1); 
    }
    std::vector<double> v2;
    v2.reserve(size);
    for (int i = 0; i < size; i++) {
        v1[i] = 400.0/(i+1); 
    }
    std::vector<double> vr;
    vr.reserve(size);

    // Timed
    for (auto _ : state) {
        for (int i = 0; i < size; i++) {
            vr[i] = v1[i] * v2[i];
        }
    }
}
// Register the function as a benchmark
BENCHMARK(BM_DoubleVector);

BENCHMARK_MAIN();



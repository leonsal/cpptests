#include <string>
#include <chrono>
#include <ctime>
#include "fmt/core.h"
#include "fmt/format.h"
#include "fmt/printf.h"

// Prints formatted error message.
void vlog(const char *format, fmt::format_args args) {

    std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::string s(30, '\0');
    std::strftime(&s[0], s.size(), "%Y-%m-%d %H:%M:%S: ", std::localtime(&now));
    fmt::print(s);
    fmt::vprint(format, args);
    fmt::print("\n");
}

template <typename... Args>
void logDebug(const char *format, const Args & ... args) {
#ifndef NDEBUG
    vlog(format, fmt::make_format_args(args...));
#endif
}

int main() {

    fmt::print("fmt library test\n");

    // printf API
    std::string string1 = "string1";
    const char* charp = "char pointer";
    fmt::printf("Printing int:%d float:%f string:%s charp:%s \n", -3, 4.34, string1, charp);

    // print
    std::string formatted = fmt::sprintf("Formatted values: %d, %3.1f, %s\n", 34, 99.3, "test"); 
    fmt::print("string:{} number:{} bool:{}\n", formatted, 2, false);
   
    // user variadic function
    std::string path{"test"};
    logDebug("message:{} number:{} bool:{}", path, 1.2, false);
}

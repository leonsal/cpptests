#include <iostream>
#include <chrono>
#include "thread_pool.h"

class Foo {

public:
    void accum() {
        sum += 1;
    }

private:
    int sum{};
};



int main() {

    ThreadPool p(3) ;
    p.pushJob([]() {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        std::cout << "job1\n";  
    });
    p.pushJob([]() {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        std::cout << "job2\n";  
    });
    p.pushJob([]() {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        std::cout << "job3\n";  
    });
    //std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    p.shutdown();

    //Foo f1;
    //ThreadPool p2;
    //p2.pushJob(Foo::accum);

}


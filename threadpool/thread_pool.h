#pragma once
#include <iostream>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

class ThreadPool {

using jobfn = void(*)();

public:

    // Default constructor
    ThreadPool() {

        const auto nthreads = std::thread::hardware_concurrency();
        createThreads(nthreads);
    }

    // Constructor from number of threads
    explicit ThreadPool(int nthreads) {

        createThreads(nthreads);
    }

    ~ThreadPool() {
        if (!mTerminate.load()) {
            shutdown();
        }
    }

    // No copy constructor
    ThreadPool(const ThreadPool&) = delete;

    // No move constructor
    ThreadPool(ThreadPool&&) = delete;

    // No assignment operator
    auto operator = (const ThreadPool&) = delete;

    // No move assignment operator
    auto operator = (const ThreadPool&&) = delete;

    // Push a new job into the queue to be executed
    void pushJob(jobfn job) {

        {
            std::unique_lock<std::mutex> lock(mMut);
            mQueue.push(job);
        }
        mCond.notify_one();
    }

    // Shutdown the thread pool
    void shutdown() {
       
        mTerminate.store(true);
        mCond.notify_all();

        // Join all threads
        for (auto &t : mThreads) {
            t.join();
        }
        mThreads.clear();
    }

    
private:

    void createThreads(int nthreads) {

        for (std::size_t i = 0; i < nthreads; i++) {
            mThreads.emplace_back(&ThreadPool::workLoop, this);
        }
    }

    void workLoop() {

        while (true) {
            jobfn job;
            {
                std::unique_lock<std::mutex> locker(mMut);
                mCond.wait(locker, [this](){return !mQueue.empty() || mTerminate;});
                if (mQueue.empty()) {
                    break;
                }
                job = mQueue.front();
                mQueue.pop();
            }
            job();
        }
        std::cout << "finished\n";
    }


    std::vector<std::thread>    mThreads;
    std::mutex                  mMut;
    std::condition_variable     mCond;
    std::queue<jobfn>           mQueue;
    std::atomic<bool>           mTerminate{false};
};



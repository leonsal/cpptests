cmake_minimum_required(VERSION 3.10 FATAL_ERROR)

project(functional)

find_package(Threads)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR})
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_executable(functional main.cpp)
target_link_libraries(functional tbb Threads::Threads)




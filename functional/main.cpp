#include <iostream>
#include <chrono>
#include <vector>
#include <numeric>
#include <functional>
#include <execution>  // NEEDS LIBTBB -> sudo apt install libtbb-dev

template <typename T>
void dump(std::vector<T> v) {

    for (auto e: v) {
        std::cout << e << ",";
    }
    std::cout << "\n";
}

void accumulate_reduce() {

    const std::vector<double> v(10'000'000, 0.5);

    {
        const auto t1 = std::chrono::high_resolution_clock::now();
        const double result = std::accumulate(v.cbegin(), v.cend(), 0.0);
        const auto t2 = std::chrono::high_resolution_clock::now();
        const std::chrono::duration<double, std::milli> ms = t2 - t1;
        std::cout << std::fixed << "std::accumulate result " << result << " took " << ms.count() << "ms \n"; 
    }
    {
        const auto t1 = std::chrono::high_resolution_clock::now();
        const double result = std::reduce(std::execution::par, v.cbegin(), v.cend(), 0.0);
        const auto t2 = std::chrono::high_resolution_clock::now();
        const std::chrono::duration<double, std::milli> ms = t2 - t1;
        std::cout << std::fixed << "std::reduce result " << result << " took " << ms.count() << "ms \n"; 
    }
}

void accumulate_count_newlines() {

    std::string text{"line1\nline2\nline3\nline4\n"};
    auto count = std::accumulate(text.cbegin(), text.cend(), 0, [](int prev_count, char c) {
        return (c != '\n') ? prev_count : prev_count + 1;
    });
    std::cout << std::fixed << "std::accumulate newlines: " << count << "\n"; 
}

void string_trimming() {

    auto trim_left = [](std::string s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](char c){return (c != ' ');}));
        return s;
    };

    auto trim_right = [](std::string s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](char c){return (c != ' ');}).base(), s.end());
        return s;
    };

    auto trim = [=](std::string s) {
        return trim_left(trim_right(std::move(s)));
    };

    std::string text{"    this is a text    "}; 
    std::cout << std::fixed << "trim_left :" << "|" << trim_left(text) << "|\n"; 
    std::cout << std::fixed << "trim_right:" << "|" << trim_right(text) << "|\n"; 
    std::cout << std::fixed << "trim      :" << "|" << trim(text) << "|\n"; 
}

void partition() {

    // partition separates the vector in two groups using the specified predicate
    // The first group satisfies the predicate.
    // return an iterator to the first element of the second group.
    std::vector<int> vtest{1,2,3,4,5,6,7,8,9,0};
    auto v1 = vtest;
    std::partition(v1.begin(), v1.end(), [](int e) {return e % 2 == 0;});
    dump(v1);

    // stable_partition maintains the relative initial order of the groups.
    auto v2 = vtest;
    std::stable_partition(v2.begin(), v2.end(), [](int e) {return e % 2 == 0;});
    dump(v2);
}

void filter() {

    auto isodd  = [](int e) {return e % 2 != 0;};
    auto iseven = [](int e) {return e % 2 == 0;};

    // Remove odd numbers modyfing the original collection
    // remove_if move elements to remove to the end of the vector and
    // returns a interator to the first element to remove
    std::vector<int> v{1,2,3,4,5,6,7,8,9,0};
    v.erase(std::remove_if(v.begin(), v.end(), isodd), v.end());
    std::cout << "removed odd elements:\n";
    dump(v);

    // Remove odd numbers copying even elements to a new collection
    std::vector<int> v2{1,2,3,4,5,6,7,8,9,0};
    std::vector<int> veven;
    std::copy_if(v2.begin(), v2.end(), std::back_inserter(veven), iseven), 
    std::cout << "copied even elements:\n";
    dump(veven);
}

void transform() {

    struct Person{std::string name; int age;};
    std::vector<Person> persons{{"jon", 10}, {"boby", 8}, {"gina", 16}, {"lulu", 14}, {"baby", 2}, {"kito", 13}};
    auto filter = [](const Person& p){return p.age > 12;};

    // Generate a vector with the names of persons older then 12
    std::vector<Person> volder;
    std::copy_if(persons.begin(), persons.end(), std::back_inserter(volder), filter);
    std::vector<std::string> names(volder.size());
    std::transform(volder.cbegin(), volder.cend(), names.begin(), [](Person p) ->std::string {return p.name;});
    std::cout << "person names older than 12:\n";
    dump(names);


    // Using a folding predicate
    auto append_name_if = [=](std::vector<std::string> prev, const Person& person) -> std::vector<std::string> {
       if (filter(person)) {
            prev.push_back(person.name);
       }
       return prev;
    };
    auto res = std::accumulate(persons.begin(), persons.end(), std::vector<std::string>{}, append_name_if);
    std::cout << "person names older than 12 (using folding predicate):\n";
    dump(res);
}

auto sumints(int a, int b) {

    auto res = a + b;
    std::cout << "automatic return type deduction:" << res << "\n";
    return res;
}

class function_object {
public:
    int operator()(int a, int b) const {
        auto res = a + b;
        std::cout << "function object:" << res << "\n";
        return res;
    }
};

void generalized_lambda_capture() {

    // sum is an internal variable of the lambda (not captured)
    // and can be changed.
    auto lambda = [ sum = 0 ](int x) mutable { sum += x; return sum; };
    std::cout << "generalized lambda::" << lambda(1) << "," << lambda(1) << "," << lambda(1) << "\n";
}

void generic_lambda() {

    struct Person{std::string name; int age;};
    std::vector<Person> persons{{"jon", 32},{"kit", 16},{"roger", 42}};

    struct Animal{const char* name; int age;};
    std::vector<Animal> animals{{"biju", 8},{"turtle", 22}};

    // Works with any object which has an 'age' member
    auto predicate = [limit = 18](auto&& object) {
        return object.age > limit;
    };

    std::cout << "adult persons:" << std::count_if(persons.cbegin(), persons.cend(), predicate) << "\n";
    std::cout << "adult animals:" << std::count_if(animals.cbegin(), animals.cend(), predicate) << "\n";
}


// TODO check source from book
//template <typename Function, typename SecondArgType>
//class PartialApplication {
//
//public:
//    PartialApplication(Function function, SecondArgType arg): mFunction(function), mSecondArg(arg) {
//    }
//
//    template <typename FirstArgType>
//    auto operator()(FirstArgType&& first_arg) const -> decltype(mFunction(std::forward<FirstArgType>(first_arg), mSecondArg)) {
//        return m_function(std::forward<FirstArgType>(first_arg), mSecondArg);
//    }
//
//private:
//    Function        mFunction;
//    SecondArgType   mSecondArg;
//};

void bind_function() {

    auto is_greater_than_42 = std::bind(std::greater<double>(), std::placeholders::_1, 42);
    auto is_less_than_42    = std::bind(std::greater<double>(), 42, std::placeholders::_1);

    std::cout << "greater_than_42(100):" << is_greater_than_42(100)  << "\n";
    std::cout << "greater_than_42(10):"  << is_greater_than_42(10)  << "\n";
    std::cout << "less_than_42(10):"     << is_less_than_42(10)  << "\n";
    std::cout << "less_than_42(100):"    << is_less_than_42(100)  << "\n";
}


int main() {

    accumulate_reduce();
    accumulate_count_newlines();
    string_trimming();
    partition();
    filter();
    transform();
    sumints(1, 2);
    function_object f; f(1,2);
    generalized_lambda_capture();
    generic_lambda();
    bind_function();
}

#pragma once
#include <cassert>
#include <cmath>
#include "node.h"

template <typename T>
class Adder : public Node {

public:
    Adder(Graph& graph, int ninps, std::size_t frameSize):
        Node{graph, "adder"},
        mFrameSize(frameSize) {

        // Create inputs named from "0"
        for (int i = 0; i < ninps; i ++) {
            auto inp = new InputVector<T>(std::to_string(i));
            addInput(inp);
            mInputs.push_back(inp);
        }

        // Initialize internal output buffer
        mBuf.resize(frameSize);

        // Sets the task execution function
        //setExec([this](){this->run();});
    }

    ~Adder() = default;

    void run() {

        // For all input vectors
        for (size_t i = 0 ; i < mInputs.size(); i++) {
            auto pvec = mInputs[i]->read();
            // If first input vector, copy the its data to the output buffer
            if (i == 0) {
                mBuf = *pvec;
                continue;
            }
            // Sum each element of input buffer to output buffer
            for (size_t j = 0; j < mFrameSize; j++) {
                mBuf[j] += (*pvec)[j];
            }
        }
        sendOutput(&mBuf);
    }


private:
    size_t          mFrameSize;
    std::vector<T>  mBuf;
    std::vector<InputVector<T>*> mInputs;
};


#pragma once
#include <cassert>
#include "input.h"
#include "graph.h"

class Node {

public:
    /**
     * Constructor from graph reference and Node name
     */
    Node(Graph& graph, std::string name):
        mGraph{graph},
        mName{std::move(name)} {

        graph.addNode(this);
        // Sets the cpp taskflow task
        auto wrapper = [this]() {
            this->run();
        };
        mTask = mGraph.taskflow().emplace(wrapper);
        mTask.name(mName);
    }

    /**
     * Virtual destructor
     */
    virtual ~Node() {

        // Destroy inputs
        for (auto inp : mInps) {
            delete inp;
        }
        mInps.clear();
    }

    /**
     * Virtual method that could be overidden by derived classes
     * which starts the node processing in its own thread.
     */
    virtual void start() {}

    /**
     * Virtual method that could be overidden by derived classes
     * which stops the node processing in its own thread.
     */
    virtual void stop() {}

    /**
     * Virtual method that must be implemented by derived classes
     * and which runs the node processing
     */
    virtual void run() = 0;

    /**
     * Returns this node's name
     */
    std::string name() const {

        return mName;
    }

    /**
     * Sets the name of this Node
     */
    void setName(std::string name) {

        mName = std::move(name);
        // Task could not be created yet or never created
        if (!mTask.empty()) {
            mTask.name(mName);
        }
    }

    /**
     * Connects this node output with the specified input
     */
    void connect(Input* inp) {

        assert(inp != nullptr && "Input pointer must not be nullptr");
        mOuts.push_back(inp);

        // If the Node being connected to has a non-empty Task
        // indicates to cpp-taskflow that this Node precedes it.
        // A Node which has its own thread (not managed by cpp-taskflow)
        // has its task empty.
        if (!inp->node()->task().empty()) {
            mTask.precede(inp->node()->task());
        }
    }

    /**
     * Adds an input to this node
     * The input name must be unique
     */
    void addInput(Input* inpnew) {

        assert(inpnew != nullptr && "Input pointer must not be nullptr");
        // Checks if input name is unique
        for (auto inp : mInps) {
            if (inp->name() == inpnew->name()) {
                throw std::invalid_argument("Input name already used");
            }
        }
        inpnew->setNode(this);
        mInps.push_back(inpnew);
    }

    /**
     * Find and returns an input with the specified name or nullptr if not found
     */
    Input* input(const std::string& name) const {
        for (auto inp : mInps) {
            if (inp->name() == name) {
                return inp;
            }
        }
	    return nullptr;
    }

    /**
     * Returns const reference for vector with all this node's inputs.
     */
    const std::vector<Input*>& inputs() const {

        return mInps;
    }

    /**
     * Returns const reference for vector with all this node's outputs
     */
    const std::vector<Input*>& outputs() const {

        return mOuts;
    }

    /**
     * Returns the cpp taskflow Task associated with the Node
     * @return
     */
    tf::Task& task() {

        return mTask;
    }

    /**
     * Writes specified buffer pointer to connected inputs of other nodes
     */
    template <typename T>
    void sendOutput(std::vector<T>* buf) {

        for (auto inp: mOuts) {
            auto inpv = dynamic_cast<InputVector<T>*>(inp);
            assert(inpv != nullptr && "Connected input with different type");
            inpv->write(buf);
        }
    }

protected:
    Graph&              mGraph; // Graph which owns this node
    std::string         mName;  // node name
    std::vector<Input*> mInps;  // list of this node's inputs
    std::vector<Input*> mOuts;  // list of other node's inputs connected to this node output
    tf::Task            mTask;  // taskflow task
};


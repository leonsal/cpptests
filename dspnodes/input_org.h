#pragma once
#include <string>
#include <vector>
#include <mutex>


// Forward reference to Node class
class Node;

/**
 * Base class for all inputs
 */
class InputBase {

public:    
    /**
     * Constructor from name
     */
    InputBase(const std::string& name, Node* node=nullptr):
        mName{name}, mNode{node} {
    }

    /**
     * Virtual destructor
     */
    virtual ~InputBase() = default;

    /**
     * Deleted copy constructor
     */
    InputBase(const InputBase&) = delete;

    /**
     * Deleted copy assignment operator
     */
    InputBase& operator=(const InputBase&) = delete;

    /**
     * Returns the name of this input
     */
    std::string name() const {
        return mName;
    }

    /**
     * Returns pointer to the Node which owns this input
     */
    Node* node() const {
        return mNode;
    }

    /**
     * Returns title text
     */
    std::string title() const {
        return mTitle;
    }

    /**
     * Returns help text
     */
    std::string helpText() const {
        return mHelpText;
    }

    void setNode(Node* node) {
        mNode = node;
    }

    void setTitle(const std::string& title) {
        mTitle = title;
    }

    void setHelpText(const std::string& text) {
        mHelpText = text;
    }


private:
    std::string mName;              // input name (unique in the Node)
    Node*       mNode{nullptr};     // pointer to Node which owns this Input
    std::string mTitle;             // title string equal to name by default
    std::string mHelpText;          // optional help text
};


/**
 * Generic input with no synchronization
 */
template <typename T>
class Input : public InputBase {

public:    
    /**
     * Constructor from input name, Node pointer and vector size
     */
    Input(const std::string& name, Node* node, size_t size): InputBase{name, node} {
        mValue = new std::vector<T>(size, 0);
    }

    /**
     * Read scalar
     */
    T read() const {
        return (*mValue)[0];
    }

    void write(T val) {
        (*mValue)[0] = val;
    }

    std::vector<T>* readv() const {
        return mValue;
    }

    void writev(std::vector<T>* val) {
        mValue = val;
    }

    const T* minValue() const {
        return mMinValue;
    }

    void setMinValue(T min) {
        if (mMinValue == nullptr) {
            mMinValue = new T;
        }
        *mMinValue = min;
    }

    void setMaxValue(T max) {
        if (mMaxValue == nullptr) {
            mMaxValue = new T;
        }
        *mMaxValue = max;
    }

protected:
    std::vector<T>* mValue;
    T*              mMinValue;  // minimum input value for scalar input (may be nullptr)
    T*              mMaxValue;  // maximum input value for scalar input (may be nullptr)
    T*              mStep;      // optional step for scalar input value UI (may be nullptr)
};


/**
 * Generic input with mutex synchronization
 */
template <typename T>
class InputSync : public Input<T> {

public:
    /**
     * Constructor from input name, Node pointer and vector size
     */
    InputSync(const std::string& name, Node* node, size_t size): Input<T>{name, node, size} {
    }

    /**
     * Read scalar value
     */
    T read() {
        std::scoped_lock guard(mMut);
        return (*Input<T>::mValue)[0];
    }

    /**
     * Write scalar value
     */
    void write(T val) {
        std::scoped_lock guard(mMut);
        (*Input<T>::mValue)[0] = val;
    }

    /**
     * Read vector value
     */
    std::vector<T>* readv() {
        std::scoped_lock guard(mMut);
        return Input<T>::mValue;
    }

    /**
     * Write vector value
     */
    void writev(std::vector<T>* val) {
        std::scoped_lock guard(mMut);
        Input<T>::mValue = val;
    }

private:
    std::mutex      mMut;
};



#pragma once
#include <iostream>
#include <atomic>
#include "taskflow/taskflow.hpp"
#include "taskflow/core/observer.hpp"


class Node;
class Graph {

public:
    Graph();
    ~Graph();
    tf::Taskflow& taskflow();

    void start(size_t count=0);
    void stop();
    void wait();
    bool running() const;
    void addNode(Node* node);

    void startTrace();
    void stopTrace();
    void dumpTrace(std::ostream& out);
    void clearTrace();
    bool isTracing();


private:
    tf::Executor            mExecutor;
    tf::Taskflow            mTaskflow;
    std::shared_ptr<tf::ChromeObserver> mObserver{};
    std::atomic<bool>       mRun;
    std::vector<Node*>      mNodes;
};


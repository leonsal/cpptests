#pragma once
#include <string>
#include <vector>
#include <mutex>


// Forward reference to Node class
class Node;

/**
 * Base class for all inputs
 */
class Input {

public:    
    /**
     * Constructor from name
     */
    Input(const std::string& name):
        mName{name} {
    }

    /**
     * Virtual destructor
     */
    virtual ~Input() = default;

    /**
     * Deleted copy constructor
     */
    Input(const Input&) = delete;

    /**
     * Deleted copy assignment operator
     */
    Input& operator=(const Input&) = delete;

    /**
     * Returns the name of this input
     */
    std::string name() const {
        return mName;
    }

    /**
     * Returns pointer to the Node which owns this input
     */
    Node* node() const {
        return mNode;
    }

    /**
     * Returns title text
     */
    std::string title() const {
        return mTitle;
    }

    /**
     * Returns help text
     */
    std::string helpText() const {
        return mHelpText;
    }

    /**
     * Sets the node pointer
     */
    void setNode(Node* node) {
        mNode = node;
    }

    /**
     * Sets the Input title for UI
     */
    void setTitle(const std::string& title) {
        mTitle = title;
    }

    /**
     * Sets the Input help text for UI
     */
    void setHelpText(const std::string& text) {
        mHelpText = text;
    }

    void setEnumNames(const std::vector<const char*>& names) {

        mEnumNames = names;
    }

private:
    std::string mName;                      // input name (unique in the Node)
    Node*       mNode{nullptr};             // pointer to Node which owns this Input
    std::string mTitle;                     // title string equal to name by default
    std::string mHelpText;                  // optional help text
    std::vector<const char*> mEnumNames;    // list of possible enumeration names (may be empty)
};


/**
 * Generic scalar input
 */
template <typename T>
class InputScalar : public Input {

public:
    /**
     * Constructor from name
     */
    InputScalar(const std::string& name): Input{name} {
    }

    /**
     * Virtual destructor
     */
    virtual ~InputScalar() {
        if (mMinValue) {
            delete mMinValue;
            mMinValue = nullptr;
        }
        if (mMaxValue) {
            delete mMaxValue;
            mMaxValue = nullptr;
        }
        if (mStep) {
            delete mStep;
            mStep = nullptr;
        }
    }

    /**
     * Read scalar value
     */
    T read() {
        std::scoped_lock guard(mMut);
        return mValue;
    }

    /**
     * Write scalar value
     */
    void write(T val) {
        std::scoped_lock guard(mMut);
        mValue = val;
    }

    const T* maxValue() {
        return mMaxValue;
    }

    const T* minValue() {
        return mMinValue;
    }

    void setMinValue(T min) {
        if (mMinValue == nullptr) {
            mMinValue = new T;
        }
        *mMinValue = min;
    }

    void setMaxValue(T max) {
        if (mMaxValue == nullptr) {
            mMaxValue = new T;
        }
        *mMaxValue = max;
    }

    void setStep(T step) {

        if (mStep == nullptr) {
            mStep = new T;
        }
        *mStep = step;
    }

protected:
    std::mutex mMut;        // mutex to protect concurrent read/write
    T   mValue{0};          // input current value
    T*  mMinValue{nullptr}; // minimum input value for scalar input (may be nullptr)
    T*  mMaxValue{nullptr}; // maximum input value for scalar input (may be nullptr)
    T*  mStep{nullptr};     // optional step for scalar input value UI (may be nullptr)
};


template <typename T>
class InputVector : public Input {

public:
    /**
     * Constructor from name
     */
    InputVector(const std::string& name): Input{name} {
    }

    /**
     * Default constructor
     */
    virtual ~InputVector() = default;

    /**
     * Read vector
     */
    std::vector<T>* read() const {
        return mValue;
    }

    /**
     * Write vector
     */
    void write(std::vector<T>* val) {
        mValue = val;
    }

protected:
    std::vector<T>* mValue;
};


template <typename T>
class InputVector2Scalar : public Input {

public:
    /**
     * Constructor from name
     */
    InputVector2Scalar(const std::string& name, InputScalar<T>* inpTarget):
        Input{name}, mInpTarget{inpTarget} {
    }

    /**
     * Write first vector element to target scalar input
     */
    void write(std::vector<T>* val) {
        mInpTarget->write(val[0]);
    }

private:
    InputScalar<T>* mInpTarget;
};


#pragma once
#include <cassert>
#include <cmath>
#include "math.h"
#include <type_traits>
#include "node.h"


template <typename T>
class Signal : public Node {

//static_assert(std::is_floating_point<T>());

public:

    enum class Wave {
        Const    = 0,
        Sine     = 1,
        Cosine   = 2,
        Square   = 3,
        Triangle = 4,
        SawTooth = 5,
    };

    static const std::vector<const char*>& signalNames() {
        static const std::vector<const char*> names {
            "const", "sine", "cosine", "square", "triangle", "sawtooth"
        };
        return names;
    }

    Signal(Graph& graph, std::size_t frameSize):
        Node{graph, "signal"},
        mFrameSize(frameSize) {

        // Create type of waveform input
        mInpWaveform = new InputScalar<int>("waveform");
        mInpWaveform->setTitle("Waveform");
        mInpWaveform->setHelpText("Selects the waveform of the signal");
        mInpWaveform->setEnumNames(signalNames()); 
        mInpWaveform->write(int(Wave::Sine));
        addInput(mInpWaveform);

        // Create amplitude input
        mInpAmplitude = new InputScalar<double>("amplitude");
        mInpAmplitude->setTitle("Amplitude");
        mInpAmplitude->setHelpText("Sets the amplitude of the signal");
        mInpAmplitude->setStep(0.1);
        mInpAmplitude->write(1.0);
        addInput(mInpAmplitude);

        // Create offset input
        mInpOffset = new InputScalar<double>("offset");
        mInpOffset->setTitle("Offset");
        mInpOffset->setHelpText("Sets the offset of the signal");
        mInpOffset->setStep(0.1);
        mInpOffset->write(0.0);
        addInput(mInpOffset);

        // Create normalized frequency input
        mInpNormfreq = new InputScalar<double>("normfreq");
        mInpNormfreq->setTitle("Normalized frequency");
        mInpNormfreq->setHelpText("Sets the normalized frequency (frequency/sample rate)");
        mInpNormfreq->setMinValue(0.0);
        mInpNormfreq->setStep(0.0005);
        mInpNormfreq->write(0.002);
        addInput(mInpNormfreq);

        // Initialize internal output buffer
        mBuf.resize(frameSize);

        // Sets the task execution function
        //setExec([this](){this->run();});
    }

    ~Signal() = default;

    void run() {
        switch (mInpWaveform->read()) {
            case int(Wave::Const):
                genConst();
                break;
            case int(Wave::Sine):
                genSin();
                break;
            case int(Wave::Cosine):
                genCos();
                break;
            case int(Wave::Square):
                genSquare();
                break;
            case int(Wave::Triangle):
                genTriangle();
                break;
            case int(Wave::SawTooth):
                genSawTooth();
                break;
            default:
                throw std::runtime_error("Input Signal waveform");
        }
	    sendOutput(&mBuf);
    }


private:

    void genConst() {

        const auto amplitude = mInpAmplitude->read();
        const auto offset = mInpOffset->read();
        for (std::size_t i = 0; i < mFrameSize; i++) {
            mBuf[i] = amplitude + offset;
        }
    }

    void genSin() {

        const auto period = 2 * math::PI;
        const auto delta = period * mInpNormfreq->read();
        const auto amplitude = mInpAmplitude->read();
        const auto offset = mInpOffset->read();
        for (std::size_t i = 0; i < mFrameSize; i++) {
            mBuf[i] = amplitude * sin(mPhase) + offset;
            mPhase += delta;
            if (mPhase > period) {
                mPhase -= period;
            }
        }
    }

    void genCos() {

        const auto period = 2 * math::PI;
        const auto delta = period * mInpNormfreq->read();
        const auto amplitude = mInpAmplitude->read();
        const auto offset = mInpOffset->read();
        for (std::size_t i = 0; i < mFrameSize; i++) {
            mBuf[i] = amplitude * cos(mPhase) + offset;
            mPhase += delta;
            if (mPhase > period) {
                mPhase -= period;
            }
        }
    }

    void genSquare() {

        const auto period = 1.0;
        const auto delta = period * mInpNormfreq->read();
        const auto amplitude = mInpAmplitude->read();
        const auto offset = mInpOffset->read();
        for (std::size_t i = 0; i < mFrameSize; i++) {
            if (mPhase < period/2) {
                mBuf[i] = offset;
            } else {
                mBuf[i] = amplitude + offset;
            }
            mPhase += delta;
            if (mPhase > period) {
                mPhase -= period;
            }
        }
    }

    void genTriangle() {

        const auto period = 4.0;
        const auto delta = period * mInpNormfreq->read();
        const auto amplitude = mInpAmplitude->read();
        const auto offset = mInpOffset->read();
        for (std::size_t i = 0; i < mFrameSize; i++) {
            auto v = mPhase;
            if (mPhase < 1) {
                ;
            } else if (mPhase < 3) {
                v = 2 - mPhase;
            } else {
                v = mPhase - 4;
            }
            mBuf[i] = v * amplitude + offset;
            mPhase += delta;
            if (mPhase > period) {
                mPhase -= period;
            }
        }
    }

    void genSawTooth() {

        const auto period = 1.0;
        const auto delta = period * mInpNormfreq->read();
        const auto amplitude = mInpAmplitude->read();
        const auto offset = mInpOffset->read();
        for (std::size_t i = 0; i < mFrameSize; i++) {
            mBuf[i] = mPhase * amplitude + offset;
            mPhase += delta;
            if (mPhase > period) {
                mPhase -= period;
            }
        }
    }

protected:
    std::size_t             mFrameSize;
    InputScalar<double>*    mInpNormfreq;
    InputScalar<double>*    mInpAmplitude;
    InputScalar<double>*    mInpOffset;
    InputScalar<int>*       mInpWaveform;
    std::vector<T>          mBuf;
    double                  mPhase;
};




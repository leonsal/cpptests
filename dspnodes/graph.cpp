#include <map>
#include <set>
#include "graph.h"
#include "node.h"


/**
 * Constructor
 */
Graph::Graph(): mRun{false} {

}

/**
 * Destructor
 */
Graph::~Graph() {

    // Destroy nodes
    for (auto n :  mNodes) {
        delete n;
    }
    mNodes.clear();
}

/**
 * Starts the graph if not already started speciyfing the
 * number of times to run (pass 0 to run till stop())
 */
void Graph::start(size_t count) {

    if (mRun) {
        return;
    }

    mRun = true;
    // Start nodes which have their own threads
    for (const auto node: mNodes) {
        node->start();
    }

    // Run till stop() is called
    if (count == 0) {
        mExecutor.run_until(mTaskflow, [this]() { return !this->mRun; });
        return;
    }

    // Run for an specified 'count' times or stop() is called
    mExecutor.run_until(mTaskflow,
        [this, count]() mutable { return count-- == 0 || !this->mRun; },
        [this]() mutable {
            mRun = false;
            // Stop nodes which have their own threads
            for (const auto node: mNodes) {
                node->stop();
            }
        });
}

/**
 * Stops the graph and waits for it to stop.
 */
void Graph::stop() {
    
    if (!mRun) {
        return;
    }
    mRun = false;
    mExecutor.wait_for_all();

    // Stop nodes which have their own threads
    for (const auto node: mNodes) {
        node->stop();
    }
}

/**
 * Waits for the graph to stop
 */
void Graph::wait() {

    mExecutor.wait_for_all();
}

/**
 * Returns if the graph is running
 */
bool Graph:: running() const {

    return mRun;
}

/**
 * Adds a Node to this graph
 */
void Graph::addNode(Node* node) {

    mNodes.push_back(node);
}

tf::Taskflow& Graph::taskflow() {

    return mTaskflow;
}

void Graph::startTrace() {

    if (mObserver != nullptr) {
        return;
    }
    mObserver = mExecutor.make_observer<tf::ChromeObserver>();
}

void Graph::stopTrace() {

    if (mObserver == nullptr) {
        return;
    }
    mExecutor.remove_observer(mObserver);
    mObserver = nullptr;
}

void Graph::dumpTrace(std::ostream& out) {

    if (mObserver == nullptr) {
        return;
    }
    mObserver->dump(out);
}

void Graph::clearTrace() {

    if (mObserver == nullptr) {
        return;
    }
    mObserver->clear();
}

bool Graph::isTracing() {

    if (mObserver != nullptr) {
        return true;
    }
    return false;
}



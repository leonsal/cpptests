#include <iostream>
#include <complex>
#include <chrono>
#include "input.h"
#include "signal.h"
#include "adder.h"

using namespace std::literals;
using Type=std::complex<float>;

int main() {

    // Builds graph
    Graph graph;
    size_t frameSize = 8*1024;
    auto s1 = new Signal<Type>(graph, frameSize);
    auto s2 = new Signal<Type>(graph, frameSize);
    auto s3 = new Signal<Type>(graph, frameSize);
    s1->setName("signal1");
    s2->setName("signal2");
    s3->setName("signal3");

    auto adder = new Adder<Type>(graph, 3, frameSize);
    s1->connect(adder->input("0"));
    s2->connect(adder->input("1"));
    s3->connect(adder->input("2"));

    graph.startTrace();
    graph.start();

    // Sleep while the graph is running
    std::cout << "Running graph...\n";
    std::this_thread::sleep_for(2s);

    // Stops the graph
    graph.stop();

    // Saves event trace buffer to file
    const char* filename = "dgraph_trace.json";
    std::ofstream out;
    out.exceptions(std::ofstream::failbit | std::ofstream::badbit);
    try {
        out.open(filename, std::ios::out);
        graph.dumpTrace(out);
    } catch (std::exception& e) {
        std::cout << "Error writing trace buffer to:" << filename << "\n";
    }
}


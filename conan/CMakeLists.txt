 cmake_minimum_required(VERSION 2.8.12)

 project(conan_test)

 add_definitions("-std=c++11")

 include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
 conan_basic_setup()

 add_executable(conan_test main.cpp)
 target_link_libraries(conan_test ${CONAN_LIBS})


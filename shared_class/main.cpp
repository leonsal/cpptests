#include <iostream>
#include "test_class/test_class.h"

int main() {

    std::cout << "Shared class\n";

    Test* t = create("message");
    std::cout << "Message1:" << t->message() << "\n";

    t->setMessage("new message");
    std::cout << "Message2:" << t->message() << "\n";

    delete t;
}


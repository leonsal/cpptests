#pragma once
#include <string>

class Test {

    public:
        Test(const std::string& msg): mMessage{msg} {}
        std::string message() {
            return mMessage;
        }
        void setMessage(const std::string& msg) {
            mMessage = msg;
        }

    private:
        std::string mMessage;
};

Test* create(const std::string& msg);


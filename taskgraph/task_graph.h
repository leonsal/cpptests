#pragma once
#include <vector>
#include <mutex>
#include <condition_variable>
#include <unordered_map>
#include <atomic>
#include "../libs/spinlock.h"
#include "task_runner.h"
#include "task.h"

class TaskGraph {
   
public:

    // Constructor
    TaskGraph(TaskRunner* runner = nullptr);

    // Destructor
    ~TaskGraph();

    // No copy constructor
    TaskGraph(const TaskGraph&) = delete;

    // No move constructor
    TaskGraph(TaskGraph&&) = delete;

    // No assignment operator
    auto operator = (const TaskGraph&) = delete;

    // No move assignment operator
    auto operator = (const TaskGraph&&) = delete;

    // Connects src task to dest task
    void connect(Task* src, Task* dest);

    // Starts execution of the graph for the specified number of times.
    // If count is 0, executes till stop() is called.
    void start(bool pipeline=false, std::size_t cycles = 0);

    // Stops execution of the graph.
    // If wait is true, waits for all the tasks to finish execution
    void stop(bool wait=true);

    // Waits for graph to stop
    void waitStop();

    // Returns the number of cycles executed
    std::size_t cycles();

    // Dumps the task map for debugging
    void dumpTaskMap(std::ostream& out);

private:

    // Find source and sink tasks
    void findSourcesAndSinks();

    void notifyStop();

    // TaskInfo internal class wraps the user task to be scheduled and aggregates information
    // about its dependencies and runtime counters.
    struct TaskInfo : public Task {

        // Constructor
        TaskInfo(TaskGraph& graph, Task* task): mGraph{graph}, mTask{task} {}

        // Adds a task to the connection list of this task
        void addConnection(TaskInfo *tinfo);

        // Returns the name of the wrapped task
        virtual std::string name() const override;

        // Runs task if it is not already running
        void runTask();

        // Try to run this source task if possible
        void tryRunSource();

        // If all dependencies of this task were executed, schedules
        // the execution of this task
        void trySchedule();

        // Runs the wrapped task and try to schedule next task(s)
        virtual void run() override;

        TaskGraph&              mGraph;
        Task*                   mTask;
        std::vector<TaskInfo*>  mPrevSources;
        std::vector<TaskInfo*>  mConnectedTo;
        int                     mDependencies{0};
        int                     mDepsRun{0};
        spinlock                mDepsRunLock;
        std::atomic<bool>       mRunning{false};
        spinlock                mRunningLock;
    };

    TaskRunner*                             mRunner;
    bool                                    mPipeline{false};
    std::unordered_map<Task*, TaskInfo*>    mTaskMap;
    std::vector<TaskInfo*>                  mSources; 
    std::vector<TaskInfo*>                  mSinks; 
    std::atomic<int>                        mSinksRunning{0};
    spinlock                                mSinksRunningLock;
    std::atomic<bool>                       mStop{true};
    bool                                    mStopped{true};
    std::atomic<std::size_t>                mCycles{0};
    std::size_t                             mMaxCycles{0};  
    std::mutex                              mMut;
    std::condition_variable                 mCond;
};



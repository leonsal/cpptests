#include <iostream>
#include <cassert>
#include "task_graph.h"


void TaskGraph::TaskInfo::addConnection(TaskInfo* tinfo) {

    mConnectedTo.push_back(tinfo);
}

std::string TaskGraph::TaskInfo::name() const {

    return mTask->name();
}

bool TaskGraph::TaskInfo::trySchedule() {

    mDepsRunLock.lock();
    mDepsRun++;
    if (mDepsRun == mDependencies) {
        mDepsRunLock.unlock();
        mGraph.mRunner->pushTask(this);
        mDepsRun = 0;
        return true;
    }
    mDepsRunLock.unlock();
    return false;
}

void TaskGraph::TaskInfo::run() {

    mTask->run();

    // If this task has no connected tasks, it is a sink task
    if (mConnectedTo.size() == 0) {
        mGraph.mSinksRunLock.lock();
        mGraph.mSinksRun++;
        // If not all sink tasks were executed, nothing to do
        if (mGraph.mSinksRun < mGraph.mSinks.size()) {
            mGraph.mSinksRunLock.unlock();
            return;
        }
        mGraph.mSinksRunLock.unlock();
        mGraph.startCycle();
        return;
    }

    // Try to schedule connected tasks
    for (auto tinfo : mConnectedTo) {
        tinfo->trySchedule();
    }
}

TaskGraph::TaskGraph(TaskRunner* runner) {

    if (runner != nullptr) {
        mRunner = runner;
    } else {
        mRunner = &TaskRunner::get();
    }
}

TaskGraph::~TaskGraph() {

    for (const auto [task, tinfo]: mTaskMap) {
        delete tinfo;
        delete task;
    }
}

void TaskGraph::connect(Task* src, Task* dest) {

    if (!mStop || !mStopped) {
        throw std::runtime_error("TaskGraph cannot be changed while running");
    }

    // Creates entry for destination task if necessary
    auto iter = mTaskMap.find(dest);
    TaskInfo* destInfo;
    if (iter != mTaskMap.end()) {
        destInfo = iter->second;
    }
    else {
        destInfo = new TaskInfo(*this, dest);
        mTaskMap.insert({dest, destInfo});
    }

    // If source task not found in task map inserts new entry 
    // containing the dependent task (source)
    iter = mTaskMap.find(src);
    if (iter == mTaskMap.end()) {
        auto tinfo = new TaskInfo(*this, src);
        tinfo->addConnection(destInfo);
        mTaskMap.insert({src, tinfo});
    // Otherwise just adds dependent source task to destination task info
    } else {
        iter->second->addConnection(destInfo);
    }

    // Increments dependencies for destination task
    destInfo->mDependencies++;
}

void TaskGraph::start(std::size_t count) {

    if (!mStop || !mStopped) {
        return;
    }

    if (count == 0) {
        mCycles = 0;
    } else {
        mCycles = count + 1;
    }
    mStop = false;
    mStopped = false;
    findSourcesAndSinks();
    startCycle();
}

void TaskGraph::stop(bool wait) {

    if (mStop) {
        return;
    }
    mStop = true;

    // Waits for all tasks to stop
    if (wait) {
        waitStop();
    }
}

void TaskGraph::waitStop() {

    std::unique_lock<std::mutex> locker(mMut);
    mCond.wait(locker, [this](){return mStopped == true;});
    locker.unlock();
}

void TaskGraph::startCycle() {

    // If number of cycles to run was specified, decrements and checks
    if (mCycles > 0) {
        mCycles--;
        if (mCycles == 0) {
            mStop = true;
        }
    }

    // If stop was requested, indicates that graph is stopped
    // notifying possible waiting thread.
    if (mStop) {
        {
            std::scoped_lock locker(mMut);
            mStopped = true;
        }
        mCond.notify_one();
        return;
    }

    // Starts cycle scheduling source tasks.
    mSinksRun = 0;
    for (const auto source: mSources) {
        mRunner->pushTask(source);
    }
}

void TaskGraph::findSourcesAndSinks() {

    mSources.clear();
    mSinks.clear();
    for (const auto& [task, tinfo] : mTaskMap) {
        // A source has no dependencies
        if (tinfo->mDependencies == 0) {
            mSources.push_back(tinfo);
        }
        // A sink is not connected to other tasks
        if (tinfo->mConnectedTo.size() == 0) {
            mSinks.push_back(tinfo);
        }
        tinfo->mDepsRun = 0;
    }
}

void TaskGraph::dumpTaskMap(std::ostream& out) {

    // Print sources
    out << "sources: ";
    for (const auto s: mSources) {
        out << s->name() << " ";
    }
    std::cout << "\n";

    // Print sinks
    out << "sinks: ";
    for (const auto s: mSinks) {
        out << s->name() << " ";
    }
    out << "\n";

    // Print task map
    for (const auto [task, tinfo]: mTaskMap) {
        out << task->name() << " dependencies: " << tinfo->mDependencies << " connected to: ";
        for (const auto dt : tinfo->mConnectedTo) {
            out << dt->name() << " ";
        }
        out << "\n";
    }
}


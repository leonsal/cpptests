#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
#include "task_graph.h"
#include "task_runner.h"
#include "node.h"
#include "../libs/tracer.h"
#include "../libs/strutil.h"
#include "jsonxx/jsonxx.h"
#include "argh/argh.h"


TaskGraph* parseGraph(const std::string& filename, const std::string& gname) {

    // Reads graph file removing comment lines
    std::fstream file;
    file.open(filename, std::ios::in);
    if (!file.is_open()) {
        std::cout << "Error opening file: " << filename << "\n";
        return nullptr;
    }
    std::string data;
    std::string line;
    while (std::getline(file, line)) {
        if (line.empty()) {
            continue;
        }
        auto trimmed = str::trimleft(line);
        if (trimmed.rfind("#", 0) == 0) {
            continue;
        }
        data += line + "\n";
    }
    file.close();
    //std::cout << data << "\n";

    // Parses graph file contents as JSON
    jsonxx::Object graphs;
    if (!graphs.parse(data)) {
        std::cout << "Error parsing: " << filename << "\n";
        return nullptr;
    }

    // Get the specified graph by name
    if (!graphs.has<jsonxx::Object>(gname)) {
        std::cout << "Graph: " << gname << " not found \n";
        return nullptr;
    }
    auto o = graphs.get<jsonxx::Object>(gname);

    // Creates TaskGraph
    std::map<std::string, Node*> nodeMap;
    auto graph = new TaskGraph();
   
    // Create all graph nodes
    std::size_t time = o.get<jsonxx::Number>("time");
    auto nodes = o.get<jsonxx::Array>("nodes");
    for (int i = 0; i < nodes.size(); i++) {

        auto node = nodes.get<jsonxx::Object>(i);
        std::string name = node.get<jsonxx::String>("name");

        // Optional node times
        auto node_tmin = time;
        auto node_tmax = time;
        if (node.has<jsonxx::Number>("time")) {
            node_tmin = node.get<jsonxx::Number>("time");        
            node_tmax = node_tmin;
        } else
        if (node.has<jsonxx::Array>("time")) {
            auto times = node.get<jsonxx::Array>("time");
            node_tmin = times.get<jsonxx::Number>(0);
            node_tmax = times.get<jsonxx::Number>(1);
        }

        int inps = node.get<jsonxx::Number>("inps");
        int expect = 0;
        if (node.has<jsonxx::Number>("expect")) {
            expect = node.get<jsonxx::Number>("expect");
        }
        auto n = new Node(*graph, name, inps, node_tmin, node_tmax, expect);
        nodeMap.insert({name, n});
    }

    // Sets connections between nodes
    for (int i = 0; i < nodes.size(); i++) {
        auto node = nodes.get<jsonxx::Object>(i);
        std::string name = node.get<jsonxx::String>("name");
        auto res = nodeMap.find(name); 
        auto n = res->second;

        if (node.has<jsonxx::Array>("connect")) {
            auto connect = node.get<jsonxx::Array>("connect");
            for (int j = 0; j < connect.size(); j++) {
                auto dest = connect.get<jsonxx::Array>(j);
                std::string dest_name = dest.get<jsonxx::String>(0);
                std::string dest_inp  = dest.get<jsonxx::String>(1);
                auto res = nodeMap.find(dest_name);
                if (res == nodeMap.end()) {
                    std::cout << "Destination node:" << dest_name << " not found\n";
                    return nullptr;
                }
                auto destNode = res->second;
                n->connect(destNode->input(dest_inp));
            }
        }
    }
    return graph;
}

// Application usage/help string
static const char* const usage = R"(taskgraph tester
usage: >taskgraph [options] graphname
options:
-h     : Shows this help
-p     : Enable pipeline
-c <n> : Number of cycles
-tc    : Enable trace to console
)";

int main(int argc, char** argv) {

    // Parse command line options
    argh::parser cmdl;
    cmdl.add_param("c");
    cmdl.parse(argc, argv);
    // Process flags
    bool pipeline = false;
    bool traceConsole = false;
    for (auto& flag : cmdl.flags()) {
        if (flag == "h") {
            std::cout << usage;
            return 0;
        }
        if (flag == "p") {
            pipeline = true;
        }
        if (flag == "tc") {
            traceConsole = true;
        }
    }
    // Process parameters
    int cycles = 0;
    for (auto& param : cmdl.params()) {
        if (param.first == "c") {
            cycles = std::stoi(param.second);
        }
    }
    // Checks positional arguments
    auto args = cmdl.pos_args();
    if (args.size() < 2) {
        std::cout << usage;
        std::cout << "graph name must be supplied\n";
        return 1;
    }

    // Try to create specified graph name
    auto g = parseGraph("graphs.json", args[1]);
    if (g == nullptr) {
        return 1;
    }

    sdr::Tracer::get().setEventCapacity(1024*1024);
    sdr::Tracer::get().enableConsole(traceConsole);
    sdr::Tracer::get().enableRecord(true);

    // Starts the graph
    g->start(pipeline, cycles);
    g->dumpTaskMap(std::cout);
    auto timeStart = std::chrono::high_resolution_clock::now();

    // If number of cycles not specified, wait for user to press Enter
    if (cycles == 0) {
        std::string line;
        std::getline(std::cin, line);
        // Stops the graph and waits till it is stopped
        std::cout << "Stopping graph...\n";
        g->stop();
        std::cout << "Stopped.\n";
    } else {
        g->waitStop();
    }

    // Get cycles executed per second
    auto elapsed = std::chrono::high_resolution_clock::now() - timeStart;
    auto elapsedS = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() / 1000.0;
    auto cps = g->cycles() / elapsedS;
    std::cout << "Executed " << g->cycles() << " cycles in " << elapsedS << "s" << " with rate " << cps << " cycles/s\n";

    // Shuts down the task runner before deleting the graph
    TaskRunner::get().shutdown();
    delete g;

    // Saves event trace buffer to file
    const char* traceRecordFile = "trace.json";
    auto events = sdr::Tracer::get().eventCount();
    if (events > 0) {
        std::ofstream out;
        out.exceptions(std::ofstream::failbit | std::ofstream::badbit);
        try {
            out.open(traceRecordFile, std::ios::out);
            sdr::Tracer::get().dump(out);
            std::cout << "Written " << events << " events to " << traceRecordFile << "\n";
        } catch (std::exception& e) {
            std::cout << "Error writing trace buffer to:" << traceRecordFile << "\n";
        }
    }
}


#include <iostream>
#include <cassert>
#include "task_graph.h"


void TaskGraph::TaskInfo::addConnection(TaskInfo* tinfo) {

    mConnectedTo.push_back(tinfo);
}

std::string TaskGraph::TaskInfo::name() const {

    return mTask->name();
}

void TaskGraph::TaskInfo::runTask() {

    mRunningLock.lock();
    if (!mRunning) {
        mRunning = true;
        if (mConnectedTo.size() == 0) {
            mGraph.mSinksRunning++;
        }
        mRunningLock.unlock();
        mGraph.mRunner->pushTask(this);
        return;
    }
    mRunningLock.unlock();
}

void TaskGraph::TaskInfo::tryRunSource() {

    // If stop requested, do not run sources anymore
    if (mGraph.mStop) {
        return;
    }

    // Checks if all tasks connected to this source are not running
    mRunningLock.lock();
    for (const auto tk : mConnectedTo) {
        if (tk->mRunning) {
            mRunningLock.unlock();
            return;
        }
    }
    mRunningLock.unlock();
    runTask();
}

void TaskGraph::TaskInfo::trySchedule() {

    // Runs this node only if all its dependencies has runned
    mDepsRunLock.lock();
    mDepsRun++;
    if (mDepsRun == mDependencies) {
        mDepsRun = 0;
        mDepsRunLock.unlock();
        runTask();
        return;
    }
    mDepsRunLock.unlock();
}

void TaskGraph::TaskInfo::run() {

    mTask->run();
    mRunning = false;

    if (mGraph.mPipeline) {
        if (mConnectedTo.size() == 0) {
            // By convention, if this is the first sink, increments cycles counter
            if (mGraph.mSinks[0] == this) {
                mGraph.mCycles++;
                if (mGraph.mMaxCycles > 0) {
                    if (mGraph.mCycles == mGraph.mMaxCycles) {
                        mGraph.mStop = true;
                    }
                }
            }
            // If stop was requested, stops at the end of the cycle
            mGraph.mSinksRunningLock.lock();
            mGraph.mSinksRunning--;
            if (mGraph.mStop && mGraph.mSinksRunning == 0) {
                mGraph.mSinksRunningLock.unlock();
                mGraph.notifyStop();
                return;
            }
            mGraph.mSinksRunningLock.unlock();
        }

        // Try to schedule connected tasks
        for (auto tinfo : mConnectedTo) {
            tinfo->trySchedule();
        }

        // Try to schedule previous sources
        for (auto tinfo : mPrevSources) {
            tinfo->tryRunSource();
        }
        return;
    }

    // NO pipeline
    if (mConnectedTo.size() == 0) {
        mGraph.mSinksRunningLock.lock();
        mGraph.mSinksRunning--;
        if (mGraph.mSinksRunning == 0) {
            mGraph.mSinksRunningLock.unlock();
            // Checks if graph was executed by the specified number of cycles
            mGraph.mCycles++;
            if (mGraph.mMaxCycles > 0) {
                if (mGraph.mCycles == mGraph.mMaxCycles) {
                    mGraph.mStop = true;
                }
            }
            // Stops graph if requested
            if (mGraph.mStop) {
                mGraph.notifyStop();
                return;
            }
            // Run graph sources
            for (const auto source: mGraph.mSources) {
                 source->runTask();
            }
            return;
        }
        mGraph.mSinksRunningLock.unlock();
    }

    // Try to schedule connected tasks
    for (auto tinfo : mConnectedTo) {
        tinfo->trySchedule();
    }
}

TaskGraph::TaskGraph(TaskRunner* runner) {

    if (runner != nullptr) {
        mRunner = runner;
    } else {
        mRunner = &TaskRunner::get();
    }
}

TaskGraph::~TaskGraph() {

    for (const auto [task, tinfo]: mTaskMap) {
        delete tinfo;
        delete task;
    }
}

void TaskGraph::connect(Task* src, Task* dest) {

    if (!mStop || !mStopped) {
        throw std::runtime_error("TaskGraph cannot be changed while running");
    }

    // Creates entry for destination task if necessary
    auto iter = mTaskMap.find(dest);
    TaskInfo* destInfo;
    if (iter != mTaskMap.end()) {
        destInfo = iter->second;
    }
    else {
        destInfo = new TaskInfo(*this, dest);
        mTaskMap.insert({dest, destInfo});
    }

    // If source task not found in task map inserts new entry 
    // containing the dependent task (source)
    iter = mTaskMap.find(src);
    if (iter == mTaskMap.end()) {
        auto tinfo = new TaskInfo(*this, src);
        tinfo->addConnection(destInfo);
        mTaskMap.insert({src, tinfo});
    // Otherwise just adds dependent source task to destination task info
    } else {
        iter->second->addConnection(destInfo);
    }

    // Increments dependencies for destination task
    destInfo->mDependencies++;
}

void TaskGraph::start(bool pipeline, std::size_t cycles) {

    if (!mStop || !mStopped) {
        return;
    }

    mPipeline = pipeline;
    mMaxCycles = cycles;
    mStop = false;
    mStopped = false;
    findSourcesAndSinks();

    // Starts scheduling source tasks.
    mSinksRunning = 0;
    for (const auto source: mSources) {
        source->runTask();
    }
}

void TaskGraph::stop(bool wait) {

    if (mStop) {
        return;
    }
    mStop = true;

    // Waits for all tasks to stop
    if (wait) {
        waitStop();
    }
}

void TaskGraph::waitStop() {

    std::unique_lock<std::mutex> locker(mMut);
    mCond.wait(locker, [this](){return mStopped == true;});
    locker.unlock();
}

std::size_t TaskGraph::cycles() {
    
    return mCycles;
}

void TaskGraph::findSourcesAndSinks() {

    mSources.clear();
    mSinks.clear();
    for (const auto& [task, tinfo] : mTaskMap) {
        // A source has no dependencies
        if (tinfo->mDependencies == 0) {
            mSources.push_back(tinfo);
        }
        // A sink is not connected to other tasks
        if (tinfo->mConnectedTo.size() == 0) {
            mSinks.push_back(tinfo);
        }
        tinfo->mDepsRun = 0;
    }
    // Appends source nodes to each node wich depends on this source
    for (const auto src : mSources) {
        for (const auto dest : src->mConnectedTo) {
            dest->mPrevSources.push_back(src);
        }
    }
}

void TaskGraph::notifyStop() {

    {
       std::scoped_lock locker(mMut);
       mStopped = true;
    }
    mCond.notify_one();
}

void TaskGraph::dumpTaskMap(std::ostream& out) {

    // Print sources
    out << "sources: ";
    for (const auto s: mSources) {
        out << s->name() << " ";
    }
    std::cout << "\n";

    // Print sinks
    out << "sinks: ";
    for (const auto s: mSinks) {
        out << s->name() << " ";
    }
    out << "\n";

    // Print task map
    for (const auto [task, tinfo]: mTaskMap) {
        out << task->name() << " dependencies: " << tinfo->mDependencies;
        out << " prev_sources: ";
            for (const auto dt : tinfo->mPrevSources) {
                out << dt->name() << " ";
            }
        out << "\tconnected to: ";
        for (const auto dt : tinfo->mConnectedTo) {
            out << dt->name() << " ";
        }
        out << "\n";
    }
}


#pragma once
#include <string>
#include <vector>
#include <mutex>
#include "../libs/tracer.h"


// Forward reference to Node class
class Node;

// Base class for all inputs
class InputBase {

public:    
     // Constructor from name
    InputBase(const std::string& name):
        mName{name} {
    }

    // Virtual destructor
    virtual ~InputBase() = default;

    // No copy constructor
    InputBase(const InputBase&) = delete;

    // No copy assignment operator
    InputBase& operator=(const InputBase&) = delete;

    // Returns the name of this input
    std::string name() const {
        return mName;
    }

    // Returns pointer to the Node which owns this input
    Node* node() const {
        return mNode;
    }
   
    // Sets the node pointer
    void setNode(Node* node) {
        mNode = node;
    }

private:
    std::string mName;                      // input name (unique in the Node)
    Node*       mNode{nullptr};             // pointer to Node which owns this Input
};


// Generic non-synchronized input
template <typename T>
class Input : public InputBase {

public:

    // Constructor from name
    Input(const std::string& name): InputBase{name} {}

    // Virtual destructor
    virtual ~Input() override {}

    // Read value
    T read() {
        //TRACE_INSTANT("input", "read", sdr::Tracer::Scope::Default);
        return mValue;
    }

    // Write scalar value
    void write(T val) {
        //TRACE_INSTANT("input", "write", sdr::Tracer::Scope::Default);
        mValue = val;
    }

protected:
    T   mValue{0};          // input current value
};



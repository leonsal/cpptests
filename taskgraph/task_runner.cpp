#include "task_runner.h"

TaskRunner& TaskRunner::get() {

    static TaskRunner instance;
    return instance;
}

TaskRunner::TaskRunner() {

    const auto nthreads = std::thread::hardware_concurrency();
    createThreads(nthreads);
}

TaskRunner::TaskRunner(int nthreads) {

    createThreads(nthreads);
}

TaskRunner::~TaskRunner() {

    if (!mTerminate) {
        shutdown();
    }
}

void TaskRunner::pushTask(Task* task) {

    {
        std::unique_lock<std::mutex> lock(mMut);
        mQueue.push(task);
    }
    mCond.notify_one();
}

void TaskRunner::shutdown() {
       
    mTerminate = true;
    mCond.notify_all();

    // Join all threads
    for (auto &t : mThreads) {
        t.join();
    }
    mThreads.clear();
}

void TaskRunner::createThreads(int nthreads) {

    for (std::size_t i = 0; i < nthreads; i++) {
        mThreads.emplace_back(&TaskRunner::workLoop, this);
    }
}

void TaskRunner::workLoop() {

    while (true) {
        Task* task;
        {
            std::unique_lock<std::mutex> locker(mMut);
            mCond.wait(locker, [this](){return !mQueue.empty() || mTerminate;});
            if (mQueue.empty()) {
                break;
            }
            task = mQueue.front();
            mQueue.pop();
        }
        task->run();
    }
}


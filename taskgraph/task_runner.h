#pragma once
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "task.h"

class TaskRunner {

public:

    // Returns single instance of TaskRunner
    static TaskRunner& get();

    // Default constructor
    TaskRunner();

    // Constructor from number of threads
    explicit TaskRunner(int nthreads);

    // Destructor
    ~TaskRunner();

    // No copy constructor
    TaskRunner(const TaskRunner&) = delete;

    // No move constructor
    TaskRunner(TaskRunner&&) = delete;

    // No assignment operator
    auto operator = (const TaskRunner&) = delete;

    // No move assignment operator
    auto operator = (const TaskRunner&&) = delete;

    // Push a new task the queue to be executed
    void pushTask(Task* task);

    // Shutdown the thread pool
    void shutdown();
       

private:

    void createThreads(int nthreads);
    void workLoop();
    std::vector<std::thread>    mThreads;
    std::queue<Task*>           mQueue;
    std::mutex                  mMut;
    std::condition_variable     mCond;
    std::atomic<bool>           mTerminate{false};
};



#pragma once
#include <string>

class Task {

public:
    virtual void run() = 0;
    virtual std::string name() const  = 0;
    virtual void start() {}
    virtual void stop() {}
    virtual ~Task() {}
};


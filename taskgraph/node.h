#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include "input.h"
#include "task_graph.h"
#include "../libs/tracer.h"

class Node : public Task {

public:

    // Constructor
    Node(TaskGraph& graph, std::string name, int ninps, std::size_t tmin, std::size_t tmax, int expected=0):
        mGraph{graph}, mName{name}, mTmin{tmin}, mTmax{tmax}, mExpected{expected} {

        for (auto i = 0; i < ninps; i++) {
            auto inp = new Input<int>(std::to_string(i));
            inp->setNode(this);
            mInputs.push_back(inp);
        }
    }

    // Destructor
    virtual ~Node() {
        for (auto inp : mInputs) {
            delete inp;
        }
        mInputs.clear();
    }

    // Returns the name of this node
    virtual std::string name() const override {
        
        return mName;
    }

    // Returns input with the specified name
    InputBase* input(const std::string& name) {

        for (const auto inp : mInputs) {
            if (inp->name() == name) {
                return inp;
            }
        }
        throw std::runtime_error("Invalid Node input name");
    }

    // Connect to specified input
    void connect(InputBase* input) {

        mOutputs.push_back(input);
        mGraph.connect(this, input->node());
    }

    // Runs the node work once
    virtual void run() override {

        auto name = mName + std::to_string(mCycle);
        TRACE_BEGIN(name, "main");

        // Read input and sums
        int sum = 1;
        for (auto& inp : mInputs) {
            sum += inp->read();
        }

        // Simulates work
        auto timeMS = mTmin;
        if (mTmax > mTmin) {
            timeMS = mTmin + (std::rand() % (mTmax - mTmin + 1));
        }
        //std::cout << "Time(" << mTmin << "," << mTmax << "):" << timeMS << "\n";
        std::this_thread::sleep_for(std::chrono::milliseconds(timeMS));

        // If no outputs, this is a sink node and checks expected result
        if (mOutputs.size() == 0) {
            if (sum != mExpected) {
                std::cout << "Node:" << mName << " --> sum:" << sum << " expected: " << mExpected  << "\n";
                throw std::runtime_error("Expected Error");
            }
        // Sends sum to connected outputs
        } else {
            for (auto& out : mOutputs) {
                dynamic_cast<Input<int>*>(out)->write(sum);
            }
        }

        mCycle++;
        if (mCycle > 9) {
            mCycle = 0;
        }
        TRACE_END(name, "main");
    }

private:
    TaskGraph&                  mGraph;
    std::string                 mName;
    std::size_t                 mTmin;
    std::size_t                 mTmax;
    int                         mExpected;
    int                         mCycle{0};
    std::vector<Input<int>*>    mInputs;
    std::vector<InputBase*>     mOutputs;
};



#include <iostream>
#include <sstream>
#include <fstream>
#include <thread>
#include <functional>
#include <chrono>
#include <atomic>
#include "tracer.h"
#include "taskflow/taskflow/taskflow.hpp"

using namespace std::literals;

void print(const std::string& msg) {

    std::ostringstream outs;
    outs << msg << ":" << std::this_thread::get_id() << "\n";
    std::cout << outs.str();
}

void task5() {
    TRACE_BEGIN("T5", "MAIN");
    std::this_thread::sleep_for(10ms);
    TRACE_END("T5", "MAIN");
}

struct Node {
    void run() {
        TRACE_BEGIN("T6", "MAIN");
        std::this_thread::sleep_for(10ms);
        TRACE_END("T6", "MAIN");
     }
};


int main() {

    tf::Executor executor;
    tf::Taskflow taskflow;

    auto t1 = taskflow.emplace([]() {
        TRACE_BEGIN("T1", "MAIN");
        std::this_thread::sleep_for(10ms);
        TRACE_END("T1", "MAIN");
    });
    auto t2 = taskflow.emplace([]() {
        TRACE_BEGIN("T2", "MAIN");
        std::this_thread::sleep_for(10ms);
        TRACE_END("T2", "MAIN");
    });
    auto t3 = taskflow.emplace([]() {
        TRACE_BEGIN("T3", "MAIN");
        std::this_thread::sleep_for(10ms);
        TRACE_END("T3", "MAIN");
    });

    // std::function
    std::function<void()> func4 = []() {
        TRACE_BEGIN("T4", "MAIN");
        std::this_thread::sleep_for(10ms);
        TRACE_END("T4", "MAIN");
    };
    auto t4 = taskflow.emplace(func4);
   
    // free function
    auto t5 = taskflow.emplace(task5);

    // member function
    Node n{};
    auto t6 = taskflow.emplace([&n](){n.run();});

    // Dependencies graph:
    // t1    t2    t5
    //       t3    t6
    //       t4  
    t1.precede(t2);
    t1.precede(t3);
    t1.precede(t4);
    t2.precede(t5);
    t2.precede(t6);
    t3.precede(t5);
    t3.precede(t6);
    t4.precede(t5);
    t4.precede(t6);

    sdr::Tracer::get().setEventCapacity(16*1024);
    sdr::Tracer::get().enableConsole(false);
    sdr::Tracer::get().enableRecord(true);
    TRACE_BEGIN("MAIN", "PROC");

    // Start flow execution
    std::atomic<bool> run = true;
    executor.run_until(taskflow, [&run](){return !run;});

    // Sleep and stop flow
    std::cout << "Cpp-Taskflow Dependency Graph Test\n";
    std::this_thread::sleep_for(2s);
    run = false;
    executor.wait_for_all();
    TRACE_END("MAIN", "PROC");

    // Saves event trace buffer to file
    const char* traceRecordFile = "dgraph_trace.json";
    auto events = sdr::Tracer::get().eventCount();
    if (events > 0) {
        std::ofstream out;
        out.exceptions(std::ofstream::failbit | std::ofstream::badbit);
        try {
            out.open(traceRecordFile, std::ios::out);
            sdr::Tracer::get().dump(out);
            std::cout << "Written " << events << " events to " << traceRecordFile << "\n";
        } catch (std::exception& e) {
            std::cout << "Error writing trace buffer to:" << traceRecordFile << "\n";
        }
    }
}

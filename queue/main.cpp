#include <iostream>
#include "queue.h"

int main() {

    std::cout << "Queue test\n";
    Queue<int> queue{100};

    // Starts producer thread
    std::thread producer([&queue](){
        int start = 0;
        size_t total = 10'000'000;
        while (total--) {
            auto data = start++;
            auto res = queue.push(data, 10);
            if (!res) {
                throw "Timeout writing item";
            }
        }
        queue.push(-1);
    });

    // Starts consumer thread
    std::thread consumer([&queue](){
        int start = 0;
        while (true) {
            int data;
            auto res = queue.pop(&data, 100);
            if (!res) {
                throw "Timeout reading item";
            }
            if (data == -1)  {
                return;
            }
            if (data != start++) {
                throw std::runtime_error("Read invalid data from queue");
            }
            if (data % 1000 == 0) {
                std::cout << "read:" << data << "\n";
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(0));
        }
    });

    // Waits for threads to finish
    producer.join();
    consumer.join();
}


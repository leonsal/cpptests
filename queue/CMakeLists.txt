cmake_minimum_required(VERSION 3.0 FATAL_ERROR)

project(queue)

find_package(Threads)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR})
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_executable(queue main.cpp)
target_link_libraries(queue Threads::Threads)



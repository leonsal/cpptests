#pragma once
#include <thread>
#include <deque>
#include <mutex>
#include <chrono>
#include <condition_variable>

template <typename TYPE>
class Queue {

    public:
        explicit Queue(std::size_t size): mSize{size} {}
        ~Queue() = default;
        Queue(const Queue&) = delete;               // No copy constructor
        Queue& operator=(const Queue&) = delete;    // No copy assignment

        // Inserts an item to the back of the queue
        // Blocks till there is space to insert the item
        void push(TYPE item) {

            std::unique_lock<std::mutex> locker(mMut);
            mCond.wait(locker, [this](){return mQueue.size() < mSize;});
            mQueue.push_back(item);
            locker.unlock();
            mCond.notify_one();
        }

        // Removes the first element from the front of the queue,
        // blocking if the queue is empty
        TYPE pop() {

            std::unique_lock<std::mutex> locker(mMut);
            mCond.wait(locker, [this](){return !mQueue.empty();});
            TYPE front = mQueue.front();
            mQueue.pop_front();
            locker.unlock();
            mCond.notify_one();
            return front;
        }

        // Inserts an item to the back of the queue,
        // blocking if there is no space available for the maximum 
        // specified timeout in milliseconds.
        // Return true if the item was inserted or false if timeout occurred.
        bool push(TYPE item, std::size_t timeoutMS) {

            std::unique_lock<std::mutex> locker(mMut);
            if (!mCond.wait_for(locker, std::chrono::milliseconds(timeoutMS), [this](){return mQueue.size() < mSize;})) {
                return false;
            }
            mQueue.push_back(item);
            locker.unlock();
            mCond.notify_one();
            return true;
        }

        // Removes the first element from the front of the queue,
        // blocking if the queue is empty for the maximum specified timeout in milliseconds.
        // Returns true if the item was read or false if timeout occurred.
        bool pop(TYPE* item, std::size_t timeoutMS) {

            std::unique_lock<std::mutex> locker(mMut);
            if (!mCond.wait_for(locker, std::chrono::milliseconds(timeoutMS), [this](){return !mQueue.empty();})) {
                return false;
            }
            *item = mQueue.front();
            mQueue.pop_front();
            locker.unlock();
            mCond.notify_one();
            return true;
        }

        // Returns the current number of elements in the queue
        std::size_t count() {
            std::unique_lock<std::mutex> locker(mMut);
            return mQueue.size();
        }


    private:
        size_t                  mSize;
        std::mutex              mMut;
        std::condition_variable mCond;
        std::deque<TYPE>        mQueue;
};


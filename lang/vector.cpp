#include <iostream>
#include "vector"

void vector() {

    std::cout << "Vector tests\n";

    std::vector<int> vec1 {1,2,3,4,5};
    std::cout << "vec1: ";
    for (auto el: vec1) {
        std::cout << el << " ";
    }
    std::cout << std::endl;
    std::cout << "vec1: size=" << vec1.size() << "\n";
    std::cout << "vec1: capacity=" << vec1.capacity() << "\n";

}


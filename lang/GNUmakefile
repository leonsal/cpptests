CLANG_COMPILERS=-DCMAKE_C_COMPILER=/usr/bin/clang -DCMAKE_CXX_COMPILER=/usr/bin/clang++
GPROF_FLAGS=-pg
ASAN_FLAGS=-fsanitize=address
TSAN_FLAGS=-fsanitize=thread
MSAN_FLAGS=-fsanitize=memory -fsanitize-memory-track-origins=2 

#
# default target: gcc debug
#
all: build_gcc_debug
	cmake --build build_gcc_debug -- -j8
	cp build_gcc_debug/compile_commands.json .

build_gcc_debug: 
	cmake -Bbuild_gcc_debug -H. -DCMAKE_BUILD_TYPE=Debug

clean:
	cmake --build build_gcc_debug --target clean
	rm -f compile_commands.json

#
# gcc debug with address sanitizer
#
asan: build_gcc_debug_asan
	cmake --build build_gcc_debug_asan -- -j8
	cp build_gcc_debug_asan/compile_commands.json .

build_gcc_debug_asan: 
	cmake -Bbuild_gcc_debug_asan -H. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS="$(ASAN_FLAGS)"

clean_asan:
	cmake --build build_gcc_debug_asan --target clean
	rm -f compile_commands.json

#
# gcc debug with thread sanitizer
#
tsan: build_gcc_debug_tsan
	cmake --build build_gcc_debug_tsan -- -j8
	cp build_gcc_debug_tsan/compile_commands.json .

build_gcc_debug_tsan: 
	cmake -Bbuild_gcc_debug_tsan -H. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS="$(TSAN_FLAGS)"

clean_tsan:
	cmake --build build_gcc_debug_tsan --target clean
	rm -f compile_commands.json

#
# gcc debug with gprof
#
gprof: build_gcc_debug_gprof
	cmake --build build_gcc_debug_gprof -- -j8
	cp build_gcc_debug_gprof/compile_commands.json .

build_gcc_debug_gprof: 
	cmake -Bbuild_gcc_debug_gprof -H. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS="$(GPROF_FLAGS)"


clean_gprof:
	cmake --build build_gcc_debug_gprof --target clean
	rm -f compile_commands.json

#
# gcc release
#
release: build_gcc_release
	cmake --build build_gcc_release -- -j8
	cp build_gcc_release/compile_commands.json .

build_gcc_release:
	cmake -Bbuild_gcc_release -H. -DCMAKE_BUILD_TYPE=Release 

clean_release:
	cmake --build build_gcc_release --target clean
	rm -f compile_commands.json

#
# gcc release with debug info
#
relwithdebinfo: build_gcc_relwithdebinfo
	cmake --build build_gcc_relwithdebinfo -- -j8
	cp build_gcc_relwithdebinfo/compile_commands.json .

build_gcc_relwithdebinfo:
	cmake -Bbuild_gcc_relwithdebinfo -H. -DCMAKE_BUILD_TYPE=Relwithdebinfo

clean_relwithdebinfo:
	cmake --build build_gcc_relwithdebinfo --target clean
	rm -f compile_commands.json

#
# gcc release with debug info and address sanitizer
#
relwithdebinfo_asan: build_gcc_relwithdebinfo_asan
	cmake --build build_gcc_relwithdebinfo_asan -- -j8
	cp build_gcc_relwithdebinfo_asan/compile_commands.json .

build_gcc_relwithdebinfo_asan:
	cmake -Bbuild_gcc_relwithdebinfo_asan -H. -DCMAKE_BUILD_TYPE=Relwithdebinfo -DCMAKE_CXX_FLAGS="$(ASAN_FLAGS)"

clean_relwithdebinfo_asan:
	cmake --build build_gcc_relwithdebinfo_asan --target clean
	rm -f compile_commands.json

#
# gcc release with debug info and thread sanitizer
#
relwithdebinfo_tsan: build_gcc_relwithdebinfo_tsan
	cmake --build build_gcc_relwithdebinfo_tsan -- -j8
	cp build_gcc_relwithdebinfo_tsan/compile_commands.json .

build_gcc_relwithdebinfo_tsan:
	cmake -Bbuild_gcc_relwithdebinfo_tsan -H. -DCMAKE_BUILD_TYPE=Relwithdebinfo -DCMAKE_CXX_FLAGS="$(TSAN_FLAGS)"

clean_relwithdebinfo_tsan:
	cmake --build build_gcc_relwithdebinfo_tsan --target clean
	rm -f compile_commands.json

#
# gcc release with debug info and gprof
#
relwithdebinfo_gprof: build_gcc_relwithdebinfo_gprof
	cmake --build build_gcc_relwithdebinfo_gprof -- -j8
	cp build_gcc_relwithdebinfo_gprof/compile_commands.json .

build_gcc_relwithdebinfo_gprof:
	cmake -Bbuild_gcc_relwithdebinfo_gprof -H. -DCMAKE_BUILD_TYPE=Relwithdebinfo -DCMAKE_CXX_FLAGS="$(GPROF_FLAGS)"

clean_relwithdebinfo_gprof:
	cmake --build build_gcc_relwithdebinfo_gprof --target clean
	rm -f compile_commands.json

#
# clang with debug info
# 
clang: build_clang_debug
	cmake --build build_clang_debug -- -j8
	cp build_clang_debug/compile_commands.json .

build_clang_debug:
	cmake -Bbuild_clang_debug -H. -DCMAKE_BUILD_TYPE=Debug  $(CLANG_COMPILERS)

clean_clang: build_clang_debug
	cmake --build build_clang_debug --target clean
	rm -f compile_commands.json

#
# clang with debug info and address sanitizer
#
clang_asan: build_clang_debug_asan
	cmake --build build_clang_debug_asan -- -j8
	cp build_clang_debug_asan/compile_commands.json .

build_clang_debug_asan:
	cmake -Bbuild_clang_debug_asan -H. -DCMAKE_BUILD_TYPE=Debug $(CLANG_COMPILERS) -DCMAKE_CXX_FLAGS="$(ASAN_FLAGS)"

clean_clang_asan: build_clang_debug_asan
	cmake --build build_clang_debug_asan --target clean
	rm -f compile_commands.json

#
# clang with debug info and thread sanitizer
#
clang_tsan: build_clang_debug_tsan
	cmake --build build_clang_debug_tsan -- -j8
	cp build_clang_debug_tsan/compile_commands.json .

build_clang_debug_tsan:
	cmake -Bbuild_clang_debug_tsan -H. -DCMAKE_BUILD_TYPE=Debug $(CLANG_COMPILERS) -DCMAKE_CXX_FLAGS="$(TSAN_FLAGS)"

clean_clang_tsan: build_clang_debug_tsan
	cmake --build build_clang_debug_tsan --target clean
	rm -f compile_commands.json

#
# clang with debug info and memory sanitizer
#
clang_msan: build_clang_debug_msan
	cmake --build build_clang_debug_msan -- -j8
	cp build_clang_debug_msan/compile_commands.json .

build_clang_debug_msan:
	cmake -Bbuild_clang_debug_msan -H. -DCMAKE_BUILD_TYPE=Debug $(CLANG_COMPILERS) -DCMAKE_CXX_FLAGS="$(MSAN_FLAGS)"

clean_clang_msan: build_clang_debug_msan
	cmake --build build_clang_debug_msan --target clean
	rm -f compile_commands.json

#
# clang with debug info and gprof sanitizer
#
clang_gprof: build_clang_debug_gprof
	cmake --build build_clang_debug_gprof -- -j8
	cp build_clang_debug_gprof/compile_commands.json .

build_clang_debug_gprof:
	cmake -Bbuild_clang_debug_gprof -H. -DCMAKE_BUILD_TYPE=Debug $(CLANG_COMPILERS) -DCMAKE_CXX_FLAGS="$(GPROF_FLAGS)"

clean_clang_gprof: build_clang_debug_gprof
	cmake --build build_clang_debug_gprof --target clean
	rm -f compile_commands.json

#
# clang release
#
clang_release: build_clang_release
	cmake --build build_clang_release -- -j8
	cp build_clang_release/compile_commands.json .

build_clang_release:
	cmake -Bbuild_clang_release -H. -DCMAKE_BUILD_TYPE=Release $(CLANG_COMPILERS)

clean_clang_release: build_clang_release
	cmake --build build_clang_release --target clean
	rm -f compile_commands.json

#
# clang release with debug info
#
clang_relwithdebinfo: build_clang_relwithdebinfo
	cmake --build build_clang_relwithdebinfo -- -j8
	cp build_clang_relwithdebinfo/compile_commands.json .

build_clang_relwithdebinfo:
	cmake -Bbuild_clang_relwithdebinfo -H. -DCMAKE_BUILD_TYPE=Relwithdebinfo $(CLANG_COMPILERS)

clean_clang_relwithdebinfo:
	cmake --build build_clang_relwithdebinfo --target clean
	rm -f compile_commands.json

#
# clang release with debug info and address sanitizer
#
clang_relwithdebinfo_asan: build_clang_relwithdebinfo_asan
	cmake --build build_clang_relwithdebinfo_asan -- -j8
	cp build_clang_relwithdebinfo_asan/compile_commands.json .

build_clang_relwithdebinfo_asan:
	cmake -Bbuild_clang_relwithdebinfo_asan -H. -DCMAKE_BUILD_TYPE=Relwithdebinfo $(CLANG_COMPILERS) -DCMAKE_CXX_FLAGS="$(ASAN_FLAGS)"

clean_clang_relwithdebinfo_asan:
	cmake --build build_clang_relwithdebinfo_asan --target clean
	rm -f compile_commands.json

#
# clang release with debug info and thread sanitizer
#
clang_relwithdebinfo_tsan: build_clang_relwithdebinfo_tsan
	cmake --build build_clang_relwithdebinfo_tsan -- -j8
	cp build_clang_relwithdebinfo_tsan/compile_commands.json .

build_clang_relwithdebinfo_tsan:
	cmake -Bbuild_clang_relwithdebinfo_tsan -H. -DCMAKE_BUILD_TYPE=Relwithdebinfo $(CLANG_COMPILERS) -DCMAKE_CXX_FLAGS="$(TSAN_FLAGS)"

clean_clang_relwithdebinfo_tsan:
	cmake --build build_clang_relwithdebinfo_tsan --target clean
	rm -f compile_commands.json

#
# clang release with debug info and gprof sanitizer
#
clang_relwithdebinfo_gprof: build_clang_relwithdebinfo_gprof
	cmake --build build_clang_relwithdebinfo_gprof -- -j8
	cp build_clang_relwithdebinfo_gprof/compile_commands.json .

build_clang_relwithdebinfo_gprof:
	cmake -Bbuild_clang_relwithdebinfo_gprof -H. -DCMAKE_BUILD_TYPE=Relwithdebinfo $(CLANG_COMPILERS) -DCMAKE_CXX_FLAGS="$(GPROF_FLAGS)"

clean_clang_relwithdebinfo_gprof:
	cmake --build build_clang_relwithdebinfo_gprof --target clean
	rm -f compile_commands.json

clean_all:
	rm -rf build_*
	rm -f compile_commands.json

help:
	$(info target                     description)
	$(info ------------------------------------------------------------------------------)
	$(info ""                         gcc debug)
	$(info asan                       gcc debug with address sanitizer)
	$(info tsan                       gcc debug with thread sanitizer)
	$(info gprof                      gcc debug with gprof profiling)
	$(info release                    gcc release)
	$(info relwithdebinfo             gcc release with debug info)
	$(info relwithdebinfo_asan        gcc release with debug info and adress sanitizer)
	$(info relwithdebinfo_tsan        gcc release with debug info and thread sanitizer)
	$(info relwithdebinfo_gprof       gcc release with debug info and gprof profiling)
	$(info clang                      clang debug)
	$(info clang_asan                 clang debug with address sanitizer)
	$(info clang_tsan                 clang debug with thread sanitizer)
	$(info clang_msan                 clang debug with memory sanitizer)
	$(info clang_gprof                clang debug with gprof profiling)
	$(info clang_release              clang release)
	$(info clang_relwithdebinfo       clang release with debug info)
	$(info clang_relwithdebinfo_asan  clang release with debug info and address sanitizer)
	$(info clang_relwithdebinfo_tsan  clang release with debug info and thread sanitizer)
	$(info clang_relwithdebinfo_gprof clang release with debug info and gprof profiling)
	$(info clean_<target>             clean specified target)
	$(info clean_all                  remove all build directories)
	$(info ------------------------------------------------------------------------------)

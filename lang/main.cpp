#include <iostream>
#include <map>
#include "vector.h"

using test_func = void(*)();

std::map<std::string, test_func> tests {
    {"vector", vector},
};


int main(int argc, char* argv[]) {

    std::cout << "Language tests:\n";
    if (argc < 2) {
        for (auto [name, _]: tests) {
            std::cout << name << " ";
        }
        std::cout << std::endl;
        return 0;
    }

    for (int i = 1; i < argc; i++) {
        auto fn = tests[argv[i]];
        if (fn == nullptr) {
            std::cout << "INVALID TEST:" << argv[i] << "\n";
            continue;
        }
        fn();
    }
}


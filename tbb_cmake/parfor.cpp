#include <iostream>
#include <cassert>
#include "tracer.h"
#include "tbb/tbb.h"


int main() {

    std::cout << "TBB Paralell for Test\n";

    sdr::Tracer::get().setEventCapacity(16*1024);
    sdr::Tracer::get().enableConsole(true);
    sdr::Tracer::get().enableRecord(true);

    const size_t size = 1024*1024;
    std::vector<int> values(size, 0);

    TRACE_BEGIN("MAIN", "MAIN");
    tbb::parallel_for((size_t)0, values.size(), (size_t)1, [&values](int i) {
        values[i] += i;
    });
    TRACE_END("MAIN", "MAIN");

    for (int i = 0; i < values.size(); i++) {
        assert(values[i] == i);
    }

}


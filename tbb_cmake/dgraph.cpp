#include <iostream>
#include <sstream>
#include <fstream>
#include <thread>
#include <chrono>
#include "tracer.h"
#include "tbb/tbb.h"


void func1(const tbb::flow::continue_msg& msg) {
    TRACE_BEGIN("T1", "TASK");
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    TRACE_END("T1", "TASK");
}

class Functor {
    public:
    void operator()(const tbb::flow::continue_msg& msg) {
        TRACE_BEGIN("T2", "TASK");
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        TRACE_END("T2", "TASK");
    }
};

int main() {

    std::cout << "TBB Dependency Graph Test\n";
    tbb::flow::graph g;

    // Free function
    auto t1 = tbb::flow::continue_node<tbb::flow::continue_msg>(g, func1);

    // Functor
    auto t2 = tbb::flow::continue_node<tbb::flow::continue_msg>(g, Functor());

    auto t3 = tbb::flow::continue_node<tbb::flow::continue_msg>(g, [](const tbb::flow::continue_msg& msg){
        TRACE_BEGIN("T3", "TASK");
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        TRACE_END("T3", "TASK");
    });

    auto t4 = tbb::flow::continue_node<tbb::flow::continue_msg>(g, [](const tbb::flow::continue_msg& msg){
        TRACE_BEGIN("T4", "TASK");
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        TRACE_END("T4", "TASK");
    });

    auto t5 = tbb::flow::continue_node<tbb::flow::continue_msg>(g, [](const tbb::flow::continue_msg& msg){
        TRACE_BEGIN("T5", "TASK");
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        TRACE_END("T5", "TASK");
    });

    auto t6 = tbb::flow::continue_node<tbb::flow::continue_msg>(g, [](const tbb::flow::continue_msg& msg){
        TRACE_BEGIN("T6", "TASK");
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        TRACE_END("T6", "TASK");
    });

    // Dependencies graph:
    // t1    t2    t5
    //       t3    t6
    //       t4  
    make_edge(t1, t2);
    make_edge(t1, t3);
    make_edge(t1, t4);
    make_edge(t2, t5);
    make_edge(t2, t6);
    make_edge(t3, t5);
    make_edge(t3, t6);
    make_edge(t4, t5);
    make_edge(t4, t6);

    sdr::Tracer::get().setEventCapacity(16*1024);
    sdr::Tracer::get().enableConsole(false);
    sdr::Tracer::get().enableRecord(true);

    TRACE_BEGIN("MAIN", "PROC");
    for (size_t i = 0; i < 50; i++) {
        t1.try_put(tbb::flow::continue_msg());
        g.wait_for_all();
    }
    TRACE_END("MAIN", "PROC");

    // Saves event trace buffer to file
    auto& tracer = sdr::Tracer::get();
    const char* traceRecordFile = "dgraph_trace.json";
    auto events = tracer.eventCount();
    if (events > 0) {
        std::ofstream out;
        out.exceptions(std::ofstream::failbit | std::ofstream::badbit);
        try {
            out.open(traceRecordFile, std::ios::out);
            sdr::Tracer::get().dump(out);
            std::cout << "Written " << events << "/" << tracer.eventCapacity() << " events to " << traceRecordFile << "\n";
        } catch (std::exception& e) {
            std::cout << "Error writing trace buffer to:" << traceRecordFile << "\n";
        }
    }
}


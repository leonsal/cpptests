#include <iostream>
#include <thread>
#include <chrono>
#include <sstream>
#include <atomic>
#include "taskflow/taskflow.hpp"
#include "tracy/Tracy.hpp"

using namespace std::literals;

void print(const std::string& msg) {

    std::ostringstream outs;
    outs << msg << ":" << std::this_thread::get_id() << "\n";
    std::cout << outs.str();
}


int main() {

    std::cout << "Tracy Test\n";

    tf::Executor executor;
    tf::Taskflow taskflow;

    // Builds Dependencies Graph:
    // t1    t2    t4   t5
    //       t3         t6

    auto t1 = taskflow.emplace([]() {
        ZoneScoped;
        print("T1");
        std::this_thread::sleep_for(10ms);
    });

    auto t2 = taskflow.emplace([]() {
        ZoneScoped;
        print("T2");
        std::this_thread::sleep_for(10ms);
    });

    auto t3 = taskflow.emplace([]() {
        ZoneScoped;
        print("T3");
        std::this_thread::sleep_for(10ms);
    });

    auto t4 = taskflow.emplace([]() {
        print("T4");
        std::this_thread::sleep_for(10ms);
    });

    auto t5 = taskflow.emplace([]() {
        print("T4");
        std::this_thread::sleep_for(10ms);
    });

    auto t6 = taskflow.emplace([]() {
        print("T4");
        std::this_thread::sleep_for(10ms);
    });


    t1.precede(t2);
    t1.precede(t3);
    t2.precede(t4);
    t3.precede(t4);
    t4.precede(t5);
    t4.precede(t6);

    // Start flow execution
    std::atomic<bool> run = true;
    executor.run_until(taskflow, [&run](){return !run;});

    // Sleep and stop flow
    std::cout << "sleeping\n";
    std::this_thread::sleep_for(5s);
    run = false;
    executor.wait_for_all();
}


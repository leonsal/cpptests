#
# Configurations
#
MSVC=-DCMAKE_C_COMPILER=cl -DCMAKE_CXX_COMPILER=cl
CLANG=-DCMAKE_C_COMPILER=clang-cl -DCMAKE_CXX_COMPILER=clang-cl

#
# default target: msvc debug
#
all: build_msvc_debug
	cmake --build build_msvc_debug
	cmake -E copy build_msvc_debug/compile_commands.json .

build_msvc_debug: 
	cmake -Bbuild_msvc_debug -H. -G "NMake Makefiles" -DCMAKE_BUILD_TYPE=Debug

clean:
	cmake --build build_msvc_debug --target clean
	cmake -E remove -f compile_commands.json

#
# msvc release
#
release: build_msvc_release
	cmake --build build_msvc_release
	cmake -E copy build_msvc_release/compile_commands.json .

build_msvc_release: 
	cmake -Bbuild_msvc_release -H. -G "NMake Makefiles" -DCMAKE_BUILD_TYPE=Release

clean_release:
	cmake --build build_msvc_release --target clean
	cmake -E remove -f compile_commands.json

#
# msvc debug using ninja build
#
ninja: build_msvc_ninja_debug
	cmake --build build_msvc_ninja_debug

build_msvc_ninja_debug:
	cmake -Bbuild_msvc_ninja_debug -H. -G "Ninja" -DCMAKE_BUILD_TYPE=Debug $(MSVC)

clean_ninja:
	cmake --build build_msvc_ninja_debug --target clean
	cmake -E remove -f compile_commands.json

#
# msvc release using ninja build
#
ninja_release: build_msvc_ninja_release
	cmake --build build_msvc_ninja_release

build_msvc_ninja_release:
	cmake -Bbuild_msvc_ninja_release -H. -G "Ninja" -DCMAKE_BUILD_TYPE=Release $(MSVC)

clean_ninja_release:
	cmake --build build_msvc_ninja_release --target clean
	cmake -E remove -f compile_commands.json

#
# clang debug using ninja build
#
clang: build_clang_debug
	cmake --build build_clang_debug

build_clang_debug:
	cmake -Bbuild_clang_debug -H. -G "Ninja" -DCMAKE_BUILD_TYPE=Debug $(CLANG)

clean_clang:
	cmake --build build_clang_debug --target clean
	cmake -E remove -f compile_commands.json

#
# clang release using ninja build
#
clang_release: build_clang_release
	cmake --build build_clang_release

build_clang_release:
	cmake -Bbuild_clang_release -H. -G "Ninja" -DCMAKE_BUILD_TYPE=Release $(CLANG)

clean_clang_release:
	cmake --build build_clang_release --target clean
	cmake -E remove -f compile_commands.json

#
# Remove all build directories
#
clean_all:
	cmake -E remove_directory build_msvc_debug
	cmake -E remove_directory build_msvc_release
	cmake -E remove_directory build_msvc_ninja_debug
	cmake -E remove_directory build_msvc_ninja_release
	cmake -E remove_directory build_clang_debug
	cmake -E remove_directory build_clang_release
	cmake -E remove -f compile_commands.json
	cmake -E remove -f sdrkit.exe

help:
	@echo "target              description"
	@echo "------------------------------------------------------------------------------"
	@echo "                    msvc debug using nmake"
	@echo "release             msvc release using nmake"
	@echo "ninja               msvc debug using ninja build"
	@echo "ninja_release       msvc release using ninja build"
	@echo "clang               clang debug using ninja build"
	@echo "clang_release       clang release using ninja build"
	@echo "clean_<target>      clean specified target"
	@echo "clean_all           remove all build directories"
	@echo "------------------------------------------------------------------------------"


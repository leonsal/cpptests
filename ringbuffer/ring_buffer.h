#pragma once
#include <cstddef>
#include <mutex>
#include <condition_variable>

template<typename TYPE>
class RingBuffer {

    public:
        /**
         * Constructs the ring buffer with the specified maximum number of elements
         */
        RingBuffer(std::size_t size):
            mSize{size},
            mCount{0},
            mData{new TYPE[size]},
            mFront{&mData[0]},
            mBack{&mData[0]},
            mEnd(&mData[size]) {}

        /**
         * Destructor
         */
        ~RingBuffer() {
            delete[] mData;
        }

        /**
         * Write data to the back of the buffer blocking if there is no space available.
         * If optional timeout in milliseconds is supplied, returns false if timeout expires.
         */
        bool write(TYPE* data, size_t len, std::size_t timeoutMS=0) {

            // Blocks if there is no space in buffer
            std::unique_lock<std::mutex> locker(mMut);
            auto checkCondition = [&](){return len <= mSize-mCount;};
            if (timeoutMS == 0) {
                mCond.wait(locker, checkCondition);
            } else {
                auto status = mCond.wait_for(locker, std::chrono::milliseconds(timeoutMS), checkCondition);
                if (!status) {
                    return false;
                }
            }

            // Copy data to buffer
            std::size_t space = mEnd - mBack;
            if (len <= space) {
                std::copy(data, data + len, mBack);
                mBack += len;
                if (mBack >= mEnd) {
                    mBack = &mData[0];
                }
            } else {
                std::copy(data, data + space, mBack);
                auto remain = len - space;
                std::copy(data + space, data + space + remain, &mData[0]);
                mBack = &mData[0] + remain;
            }
            mCount += len;

            // Unlocks buffer access and notify other threads
            locker.unlock();
            mCond.notify_all();
            return true;
        }

        /**
         * Reads data from the front of the buffer blocking if there is not enough data
         * If optional timeout in milliseconds is supplied, returns false if timeout expires.
         */
        bool read(TYPE* data, size_t len, std::size_t timeoutMS=0) {

            // Blocks if there is no data in buffer
            std::unique_lock<std::mutex> locker(mMut);
            auto checkCondition = [&](){return len <= mCount;};
            if (timeoutMS == 0) {
                mCond.wait(locker, checkCondition);
            } else {
                auto status = mCond.wait_for(locker, std::chrono::milliseconds(timeoutMS), checkCondition);
                if (!status) {
                    return false;
                }
            }

            // Copy data from buffer
            std::size_t avail;
            if (mFront < mBack) {
                avail = mBack - mFront;
            } else {
                avail = mEnd - mFront;
            }
            if (len <= avail) {
                std::copy(mFront, mFront + len, data);
                mFront += len;
                if (mFront >= mEnd) {
                    mFront = &mData[0];
                }
            } else {
                std::copy(mFront, mFront + avail, data);
                auto remain = len - avail;
                mFront = &mData[0];
                std::copy(mFront, mFront + remain, data + avail);
                mFront += remain;
            }
            mCount -= len;

            // Unlocks buffer access and notify other threads
            locker.unlock();
            mCond.notify_all();
            return true;
        }

    
    private:
        std::size_t mSize;
        std::size_t mCount;
        TYPE* mData{};
        TYPE* mFront;
        TYPE* mBack;
        TYPE* mEnd;
        std::mutex mMut;
        std::condition_variable mCond;
};



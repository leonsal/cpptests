#include <iostream>
#include <vector>
#include <cassert>
#include <thread>
#include <chrono>
#include <string>
#include <exception>
#include "ring_buffer.h"


void producer(RingBuffer<int>* rb, size_t block, size_t delayMS) {

    int next = 0;
    std::vector<int> data(block, 0);

    while (true) {
        std::cout << "producer:" << next << std::endl;
        for (size_t i=0; i< block; i++) {
            data[i] = next++;
            if (next > 1'000'000'000) {
                data[i] = 0;
            }
        }
        auto res = rb->write(&data[0], block, 2000);
        if (!res) {
            throw std::runtime_error("Producer: timeout writing");
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(delayMS));
    }
}

void consumer(RingBuffer<int>* rb, size_t block, size_t delayMS) {

    int next = 0;
    std::vector<int> data(block, 0);
    while (true) {
        std::cout << "consumer:" << next << std::endl;
        auto res = rb->read(&data[0], data.size());
        if (!res) {
            throw std::runtime_error("Consumer: timeout reading");
        }
        for (size_t i=0; i < block; i++) {
            if (data[i] != next++) {
                throw std::runtime_error("Consumer: invalid read");
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(delayMS));
    }
}


int main() {

    std::cout << "RingBuffer\n";
    RingBuffer<int> buf(64*1024);

    std::thread p(producer, &buf, 32*1023, 10);
    std::thread c(consumer, &buf, 1025, 5);

    p.join();
    c.join();
}

